package Rolls;

import Counter.Counter;
import common.StdOut;
import common.StdRandom;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: Rolls
 * Author:   cakin
 * Date:     2020/1/11
 * Description: 模拟投掷骰子
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
public class Rolls {
    public static void main( String[] args ) {
        int T = Integer.parseInt(args[0]);
        int SIDES = 6;
        Counter[] rolls = new Counter[SIDES+1];
        for(int i=1;i<=SIDES;i++){
            rolls[i] = new Counter(i+"'s");
        }
        for(int t=0;t<T;t++){
            int result = StdRandom.uniform(1,SIDES+1);
            rolls[result].increment();
        }
        for (int i=1;i<=SIDES;i++){
            StdOut.println(rolls[i]);
        }
    }
}
