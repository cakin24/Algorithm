package MyDraw;

import common.*;
import Counter.*;
import java.util.Arrays;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: MyDraw
 * Author:   cakin
 * Date:     2020/1/10
 * Description: 常用绘图
 */
public class MyDraw {
    public static void main( String[] args ) {
        //drawFunc();
        //drawRandomAarray();
        //drawRandomSortedAarray();
        drawInterval2D(".2",".5",".5",".6","10000");
    }


    /**
     * Unit tests the {@code Interval2D} data type.
     *
     * @param args the command-line arguments
     */
    public static void drawInterval2D(String... args) {
        double xmin = Double.parseDouble(args[0]);
        double xmax = Double.parseDouble(args[1]);
        double ymin = Double.parseDouble(args[2]);
        double ymax = Double.parseDouble(args[3]);
        int trials = Integer.parseInt(args[4]);

        Interval1D xInterval = new Interval1D(xmin, xmax);
        Interval1D yInterval = new Interval1D(ymin, ymax);
        Interval2D box = new Interval2D(xInterval, yInterval);
        box.draw();

        Counter counter = new Counter("hits");
        for (int t = 0; t < trials; t++) {
            double x = StdRandom.uniform(0.0, 1.0);
            double y = StdRandom.uniform(0.0, 1.0);
            Point2D point = new Point2D(x, y);

            if (box.contains(point)) counter.increment();
            else                     point.draw();
        }

        StdOut.println(counter);
        StdOut.printf("box area = %.2f\n", box.area());
    }

    public static void drawRandomSortedAarray(){
        int N = 50;
        double[] a = new double[N];
        for(int i = 0;i<N;i++){
            a[i] = StdRandom.random();
        }
        Arrays.sort(a);
        for(int i=0;i<N;i++){
            double x = 1.0*i/N;
            double y = a[i]/2.0;
            double rw=0.5/N;
            double rh = a[i]/2.0;
            StdDraw.filledRectangle(x,y,rw,rh);
        }
    }

    public static void drawRandomAarray(){
        int N = 50;
        double[] a = new double[N];
        for(int i = 0;i<N;i++){
            a[i] = StdRandom.random();
        }
        for(int i=0;i<N;i++){
            double x = 1.0*i/N;
            double y = a[i]/2.0;
            double rw=0.5/N;
            double rh = a[i]/2.0;
            StdDraw.filledRectangle(x,y,rw,rh);
        }
    }

    public static void drawFunc(){
        int N = 100;
        StdDraw.setXscale(0,N);
        StdDraw.setYscale(0,N*N);
        StdDraw.setPenRadius(0.01);
        for (int i=1;i<=N;i++){
            StdDraw.point(i,i);
            StdDraw.point(i,i*i);
            StdDraw.point(i,i*Math.log(i));
        }
    }
}
