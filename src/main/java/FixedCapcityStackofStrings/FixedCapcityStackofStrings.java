package FixedCapcityStackofStrings;

import common.StdIn;
import common.StdOut;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: FixedCapcityStackofStrings
 * Author:   cakin
 * Date:     2020/2/8 {TIME}
 * Description: 定容栈
 */
public class FixedCapcityStackofStrings {
    private String[] a;
    private int N;

    public FixedCapcityStackofStrings( int cap ) {
        a = new String[cap];
    }

    public boolean isEmpty(){
        return N == 0;
    }

    public int size(){
        return N;
    }

    public void push(String item){
        a[N++] =  item;
    }

    public String pop(){
        return a[--N];
    }

    public static void main( String[] args ) {
        FixedCapcityStackofStrings s;
        s = new FixedCapcityStackofStrings(100);
        while(!StdIn.isEmpty()){
            String item = StdIn.readString();
            if(!item.equals("-")){
                s.push(item);
            }
            else if(!s.isEmpty()) {
                StdOut.print(s.pop()+" ");
            }
        }
        StdOut.println("("+s.size()+" left on stack)");
    }
}
