package Format;

import common.StdOut;
import common.StdRandom;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: RandomSeq
 * Author:   cakin
 * Date:     2020/1/10
 * Description: Format output
 */
public class RandomSeq {
    public static void main( String[] args ) {
        int N = Integer.parseInt(args[0]);
        double lo  = Double.parseDouble(args[1]);
        double hi = Double.parseDouble(args[2]);
        for(int i=0;i<N;i++){
            double x = StdRandom.uniform(lo,hi);
            StdOut.printf("%.2f\n",x);
        }
//        System.out.println("--------------------------");
//        System.out.printf("%14d\n",512);
//        System.out.printf("%-14d\n",512);
//        System.out.printf("%14.2f\n",1595.1680010754388);
//        System.out.printf("%.7f\n",1595.1680010754388);
//        System.out.printf("%14.4e\n",1595.1680010754388);
//        System.out.printf("%14s\n","Hello,World");
//        System.out.printf("%-14s\n","Hello,World");
//        System.out.printf("%14.5s\n","Hello,World");
    }
}
