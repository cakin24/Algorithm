package Format;

import common.StdIn;
import common.StdOut;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: Average
 * Author:   cakin
 * Date:     2020/1/10
 * Description: 格式化输入
 */
public class Average {
    public static void main( String[] args ) {
        double sum = 0.0;
        int cnt = 0;
        while(!StdIn.isEmpty()){
            sum+=StdIn.readDouble();
            cnt++;
        }
        double avg = sum /cnt;
        StdOut.printf("Average is %.5f\n",avg);
    }
}
