package MyAccumulator;

import common.Accumulator;
import common.StdOut;
import common.StdRandom;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: MyAccumulator
 * Author:   cakin
 * Date:     2020/1/11
 * Description: 累加器测试
 */
public class MyAccumulator {
    public static void main( String[] args ) {
        int T = Integer.parseInt(args[0]);
        Accumulator a = new Accumulator();
        for(int t = 0;t<T;t++){
            a.addDataValue(StdRandom.uniform());
        }
        StdOut.println(a);
    }
}
