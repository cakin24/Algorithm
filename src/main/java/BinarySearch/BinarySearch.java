package BinarySearch;

import common.In;
import common.StdOut;
import java.util.Arrays;
import java.util.*;

public class BinarySearch {

    private BinarySearch() {
    }
    // 二分法查找的关键代码
    public static int indexOf( int[] a, int key ) {
        int lo = 0;
        int hi = a.length - 1;
        while (lo <= hi) {
            // Key is in a[lo..hi] or not present.
            int mid = lo + (hi - lo) / 2;
            if (key < a[mid]) hi = mid - 1;
            else if (key > a[mid]) lo = mid + 1;
            else return mid;
        }
        return -1;
    }

    public static void main( String[] args ) {
        Scanner scan = new Scanner(System.in);
        // read the integers from a file
        In in = new In(args[0]);
        int[] whitelist = in.readAllInts();

        // 排序
        Arrays.sort(whitelist);

        // 读出要找的关键字，如果没有查找到，则输出
        while(scan.hasNextInt()) {
            int key = scan.nextInt();
            if (BinarySearch.indexOf(whitelist, key) == -1)
                StdOut.println(key);
        }
    }
}

