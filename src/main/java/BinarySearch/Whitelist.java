package BinarySearch;

import common.In;
import common.StdIn;
import common.StdOut;

public class Whitelist {

    // Do not instantiate.
    private Whitelist() { }


    public static void main(String[] args) {
        In in = new In(args[0]);
        int[] white = in.readAllInts();
        StaticSETofInts set = new StaticSETofInts(white);

        // Read key, print if not in whitelist.
        while (!StdIn.isEmpty()) {
            int key = StdIn.readInt();
            if (!set.contains(key))
                StdOut.println(key);
        }
    }
}
