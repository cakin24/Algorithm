package Cat;

import common.In;
import common.Out;

import java.util.Comparator;
import java.util.Iterator;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: Cat
 * Author:   cakin
 * Date:     2020/1/11
 * Description: In和Out测试
 */
public class Cat {
    private Cat() { }
    public static void main(String[] args) {
        Out out = new Out(args[args.length - 1]);
        for (int i = 0; i < args.length - 1; i++) {
            In in = new In(args[i]);
            String s = in.readAll();
            out.println(s);
            in.close();
        }
    }
}
