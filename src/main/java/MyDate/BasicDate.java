package MyDate;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: BasicDate
 * Author:   cakin
 * Date:     2020/1/11
 * Description: Date抽象数据类型的一种实现-BasicDate
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
public class BasicDate {
    private final int month;
    private final int day;
    private final int year;

    public BasicDate( int month, int day, int year ) {
        this.month = month;
        this.day = day;
        this.year = year;
    }

    public int month() {
        return month;
    }

    public int day() {
        return day;
    }

    public int year() {
        return year;
    }

    @Override
    public String toString() {
        return month() +
                "/" + day() +
                "/" + year() ;
    }
}
