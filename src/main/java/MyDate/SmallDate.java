package MyDate;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: SmallDate
 * Author:   cakin
 * Date:     2020/1/11
 * Description: Date抽象数据类型的一种实现-SmallDate
 */
public class SmallDate {
    private final int value;
    public SmallDate(int m,int d,int y){
        value = y*512+m*32+d;
    }
    public int month(){
        return (value/32)%16;
    }
    public int day(){
        return value%32;
    }
    public int year(){
        return value/512;
    }

    @Override
    public String toString() {
        return month()+"/"+day()+"/"+year();
    }
}
