package MyDate;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: Test
 * Author:   cakin
 * Date:     2020/1/11
 * Description: Date日期测试
 */
public class Test {
    public static void main( String[] args ) {
        int m = Integer.parseInt(args[0]);
        int d = Integer.parseInt(args[1]);
        int y = Integer.parseInt(args[2]);
        BasicDate date1 = new BasicDate(m,d,y);
        SmallDate date2 = new SmallDate(m,d,y);
        System.out.println(date1);
        System.out.println(date2);
    }
}
