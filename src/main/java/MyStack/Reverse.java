package MyStack;

import common.Stack;
import common.StdIn;
import common.StdOut;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: Reverse
 * Author:   cakin
 * Date:     2020/1/17
 * Description: 整数倒序
 */
public class Reverse {
    public static void main( String[] args ) {
        Stack<Integer> stack = new Stack<Integer>();
        while(!StdIn.isEmpty()){
            stack.push(StdIn.readInt());
        }

        for(int i : stack){
            StdOut.println(i);
        }
    }
}
