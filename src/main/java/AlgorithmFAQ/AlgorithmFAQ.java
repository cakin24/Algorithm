package AlgorithmFAQ;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: AlgorithmFAQ
 * Author:   cakin
 * Date:     2020/1/11
 * Description: 算法FAQ
 */
public class AlgorithmFAQ {
    public static void main( String[] args ) {
        int index = -1;
        assert index >= 0:"Negative index in main";
        // 第一章
        test1_3();
        //test1_2();
        //test1_1();
    }

    public static void test1_3(){
        System.out.println(-14/3);
        System.out.println(14/-3);
        System.out.println(-14%3);
        System.out.println(14%-3);
    }

    public static void test1_2(){
        System.out.println(1.0/0.0);
        System.out.println(1/0);
    }

    public static void test1_1(){
        System.out.println(Math.abs(-2147483648));
    }
}
