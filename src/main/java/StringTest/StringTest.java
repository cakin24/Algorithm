package StringTest;

import common.StdIn;
import common.StdOut;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: StringTest
 * Author:   cakin
 * Date:     2020/1/11
 * Description: 常用字符串处理代码
 */
public class StringTest {
//    public static void main( String[] args ) {
//        String query = args[0];
//        while(!StdIn.isEmpty()){
//            String s = StdIn.readLine();
//            if(s.contains(query))
//                StdOut.println(s);
//        }
//    }


    public static void main( String[] args ) {
        String[] testString = {"am","amI","amIm","ay"};
        System.out.println(isSorted(testString));
        //createArrayString();
        //fileNameExtral("test.txt");
        //System.out.println(isPallinrome("123454321"));

    }

    public static boolean isSorted(String[] a){
        for(int i=1;i<a.length;i++){
            if(a[i-1].compareTo(a[i])>0){
                return false;
            }
        }
        return true;
    }

    public static void createArrayString(){
        String input = StdIn.readAll();
        String[] words = input.split("\\s+");
        for(String word:words){
            System.out.println(word);
        }
    }

    public static void fileNameExtral(String filename){
        String s = filename;
        int dot = s.indexOf(".");
        String base = s.substring(0,dot);
        String extension = s.substring(dot+1);
        System.out.println(base);
        System.out.println(extension);
    }

    public static boolean isPallinrome(String s){
        int N = s.length();
        for(int i=0;i<N/2;i++){
            if(s.charAt(i)!=s.charAt(N-1-i)){
                return false;
            }
        }
        return true;
    }
}
