/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: AlgorithmTest
 * Author:   cakin
 * Date:     2020/1/18 
 * Description: 测试文件
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
public class AlgorithmTest {
    // 码云更新代码测试
    public static void main( String[] args ) {
        System.out.println("main函数是新增的");
        System.out.println("码云上修改的代码");
    }
}
