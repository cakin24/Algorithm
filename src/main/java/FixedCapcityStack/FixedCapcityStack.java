package FixedCapcityStack;

import common.StdIn;
import common.StdOut;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: FixedCapcityStack
 * Author:   cakin
 * Date:     2020/2/8
 * Description: 泛型定容栈
 */
public class FixedCapcityStack<Item> {
    private Item[] a;
    private int N;

    public FixedCapcityStack( int cap ) {
        a = (Item[])(new Object[cap]);
    }

    public boolean isEmpty(){
        return N == 0;
    }

    public int size(){
        return N;
    }

    public void push(Item item){
        a[N++] =  item;
    }

    public Item pop(){
        return a[--N];
    }

    public static void main( String[] args ) {
        FixedCapcityStack<String> s;
        s = new FixedCapcityStack<String>(100);
        while(!StdIn.isEmpty()){
            String item = StdIn.readString();
            if(!item.equals("-")){
                s.push(item);
            }
            else if(!s.isEmpty()) {
                StdOut.print(s.pop()+" ");
            }
        }
        StdOut.println("("+s.size()+" left on stack)");
    }
}
