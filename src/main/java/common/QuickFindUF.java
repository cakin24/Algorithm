package common;

/**
 * @className: QuickFindUF
 * @description: 动态连通性问题的quick-find算法实现
 * @date: 2021/2/24
 * @author: cakin
 */
public class QuickFindUF {
    private int[] id;    // 分量id
    private int count;   // 分量数量

    /**
     * 功能描述：初始化触点
     *
     * @author cakin
     * @date 2021/2/24
     * @param n 触点个数
     */
    public QuickFindUF(int n) {
        // 初始化分量id数组
        count = n;
        id = new int[n];
        for (int i = 0; i < n; i++)
            id[i] = i;
    }

    /**
     * 功能描述：连通分量的数量
     *
     * @author cakin
     * @date 2021/2/24
     * @return int 连通分量的数量
     */
    public int count() {
        return count;
    }

    /**
     * 功能描述：p触点所在的连通分量
     *
     * @author cakin
     * @date 2021/2/24
     * @param p p触点
     * @return int 触点所在的连通分量
     */
    public int find(int p) {
        validate(p);
        return id[p];
    }

    /**
     * 功能描述：判断p触点是否有效
     *
     * @author cakin
     * @date 2021/2/24
     * @param p p触点
     */
    private void validate(int p) {
        int n = id.length;
        if (p < 0 || p >= n) {
            throw new IllegalArgumentException("index " + p + " is not between 0 and " + (n - 1));
        }
    }

    /**
     * 功能描述：判断p触点和q触点是否连通
     *
     * @author cakin
     * @date 2021/2/24
     * @param p 第1个触点
     * @param q 第2个触点
     * @return boolean 是否连通
     */
    @Deprecated
    public boolean connected(int p, int q) {
        validate(p);
        validate(q);
        return id[p] == id[q];
    }

    /**
     * 功能描述：在p和q之间添加一条连接
     *
     * @param p 第1个触点
     * @param q 第2个触点
     * @author cakin
     * @date 2021/2/24
     */
    public void union(int p, int q) {
        validate(p);
        validate(q);
        // 将p和q归并到相同的分量中
        int pID = id[p];   // needed for correctness
        int qID = id[q];   // to reduce the number of array accesses

        // 如果p和q在相同分量中，无须进行任何操作
        if (pID == qID) return;

        // 将p的分量重命名为q的名称
        for (int i = 0; i < id.length; i++) {
            if (id[i] == pID) id[i] = qID;
        }
        // 每进行一次有效归并，连通分量数量减少1
        count--;
    }

    /**
     * 功能描述：quick-find算法实现
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/2/24
     */
    public static void main(String[] args) {
        // 读取触点数量
        int n = StdIn.readInt();
        // 初始化N个分量
        QuickFindUF uf = new QuickFindUF(n);
        while (!StdIn.isEmpty()) {
            // 读取整数对
            int p = StdIn.readInt();
            int q = StdIn.readInt();
            // 如果已经连通，则忽略
            if (uf.find(p) == uf.find(q)) {
                continue;
            }
            // 归并分量
            uf.union(p, q);
            // 打印连接
            StdOut.println(p + " " + q);
        }
        // 打印分量数量
        StdOut.println(uf.count() + " components");
    }
}


