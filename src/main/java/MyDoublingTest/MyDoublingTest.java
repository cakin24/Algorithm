package MyDoublingTest;

import common.StdOut;
import common.StdRandom;
import common.Stopwatch;
import common.ThreeSum;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: MyDoublingTest
 * Author:   cakin
 * Date:     2020/2/8
 * Description: 性能实验数据分析——统计和为0的三元组的数量
 */
public class MyDoublingTest {
    private static final int MAXIMUM_INTEGER = 1000000;

    // This class should not be instantiated.
    private MyDoublingTest() { }

    public static double timeTrial(int n) {
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = StdRandom.uniform(-MAXIMUM_INTEGER, MAXIMUM_INTEGER);
        }
        Stopwatch timer = new Stopwatch();
        ThreeSum.count(a);
        return timer.elapsedTime();
    }

    public static void main(String[] args) {
        for (int n = 250; true; n += n) {
            double time = timeTrial(n);
            StdOut.printf("%7d %7.1f\n", n, time);
        }
    }
}
