package MyBag;

import common.Bag;
import common.StdIn;
import common.StdOut;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: Mystack
 * Author:   cakin
 * Date:     2020/1/17
 * Description: 计算平均值为标准差
 */
public class MyBag {
    public static void main( String[] args ) {
        Bag<Double> numbers = new Bag<Double>();
        while (!StdIn.isEmpty())
            numbers.add(StdIn.readDouble());
        int N = numbers.size();
        double sum =0.0;
        for (double x : numbers) {
            sum+=x;
        }
        double mean = sum / N;
        sum = 0.0;
        for(double x : numbers)
            sum +=(x-mean)*(x-mean);
        double std = Math.sqrt(sum/(N-1));

        StdOut.printf("Mean: %.2f\n",mean);
        StdOut.printf("Std dev: %.2f\n",std);
    }
}
