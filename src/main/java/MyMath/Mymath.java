package MyMath;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: Mymath
 * Author:   cakin
 * Date:     2020/1/6
 * Description: 我的常用数据方法
 */
public class Mymath {
    // 判断一个数是否是素数
    public static boolean isPrime(int N){
        if(N<2)
            return false;
        for(int i=2;i*i<=N;i++){
            if(N%i==0){
                return false;
            }
        }
        return true;
    }

    //计算平方根（牛顿迭代法）
    public static double sqtr(double c){
        if(c<0){
            return Double.NaN;
        }
        double err = 1e-15;
        double t = c;
        while (Math.abs(t-c/t)>err*t){
            t = (c/t+t)/2.0;
        }
        return t;
    }

    //计算调和级数
    public static double harmonic(int N){
        double sum = 0.0;
        for(int i=1;i<=N;i++){
            sum+=1.0/i;
        }
        return sum;
    }

    public static void main( String[] args ) {
        // 测试素数
        System.out.println("29是否是素数："+isPrime(29));
        // 测试平方根
        System.out.println("97的平方根是:"+sqtr(97));
        // 测试调和级数
        System.out.println("100的调和级数是："+harmonic(100));
    }
}
