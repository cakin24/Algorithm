package Flips;

import Counter.Counter;
import common.StdOut;
import common.StdRandom;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: Flips
 * Author:   cakin
 * Date:     2020/1/11
 * Description: 模拟T次投掷硬币
 */
public class Flips {
    public static void main( String[] args ) {
        int T = Integer.parseInt(args[0]);
        Counter heads = new Counter("heads");
        Counter tails = new Counter("tails");
        for (int t = 0;t<T;t++){
            if(StdRandom.bernoulli(0.5)){
                heads.increment();
            }else {
                tails.increment();
            }
        }
        StdOut.println(heads);
        StdOut.println(tails);
        StdOut.println("delta:"+Math.abs(heads.tally()-tails.tally()));
    }
}
