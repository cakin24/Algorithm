package Flips;

import Counter.Counter;
import common.StdOut;
import common.StdRandom;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: FlipsMax
 * Author:   cakin
 * Date:     2020/1/11
 * Description: 一个接受对象参数作为对象返回值的静态方法,实现投掷正反比大小
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
public class FlipsMax {
    public static Counter max(Counter x,Counter y){
        if(x.tally()>y.tally()) return x;
        else return y;
    }
    public static void main( String[] args ) {
        int T = Integer.parseInt(args[0]);
        Counter heads = new Counter("heads");
        Counter tails = new Counter("tails");
        for (int t = 0;t<T;t++){
            if(StdRandom.bernoulli(0.5)){
                heads.increment();
            }else {
                tails.increment();
            }
        }
        if(heads.tally()==tails.tally())
            StdOut.println("Tie");
        else StdOut.println(max(heads,tails)+" wins");
    }
}
