package Matrix;

/**
 * Copyright (C), 2020-2020, XXX有限公司
 * FileName: Matrix
 * Author:   cakin
 * Date:     2020/1/6
 * Description: 方阵相乘
 * a[][]*b[][]=c[][]
 * 代码中：
 * i:代表c的行
 * j：代表c的列
 * k：代表第几次进行相加运算
 */
public class Matrix {
    public static void matrix( double[][] a, double[][] b, double[][] c ) {
        int N = a.length;
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++) {
                for (int k = 0; k < N; k++) {
                    c[i][j] += a[i][k] * b[k][j];
                }
            }
    }

    public static void main( String[] args ) {
        int N = 10;
        double[][] a;
        double[][] b;
        double[][] c = new double[N][N];
        a = new double[N][N];
        b = new double[N][N];

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                a[i][j] = 1.0;
                b[i][j] = 2.0;
            }
        }
        matrix(a, b, c);
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                System.out.print(c[i][j] + " ");
            }
            System.out.println();
        }
    }
}
