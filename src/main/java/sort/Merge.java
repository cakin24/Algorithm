package sort;

import common.StdIn;
import common.StdOut;

/**
 * @className: Merge
 * @description: 自顶向下的归并排序
 * @date: 2021/2/27
 * @author: cakin
 */
public class Merge {

    public Merge() {
    }

    /**
     * 功能描述：原地归并
     *
     * @param a   待排序数组
     * @param aux 辅助数组
     * @param lo  下界
     * @param mid 下界和上界的中间位置
     * @param hi  上界限
     * @author cakin
     * @date 2021/2/27
     */
    private static void merge(Comparable[] a, Comparable[] aux, int lo, int mid, int hi) {
        // 前缀条件: a[lo .. mid] 和 a[mid+1 .. hi] 都是排好序的
        assert isSorted(a, lo, mid);
        assert isSorted(a, mid + 1, hi);

        // 初始化辅助数组
        for (int k = lo; k <= hi; k++) {
            aux[k] = a[k];
        }

        // i:左半边元素，j：右半边元素
        int i = lo, j = mid + 1;
        // 将排好序的元素放回 a 数组
        for (int k = lo; k <= hi; k++) {
            if (i > mid) a[k] = aux[j++]; // 左半边用尽，取右半边元素
            else if (j > hi) a[k] = aux[i++]; // 右半边用尽，取左半边元素
            else if (less(aux[j], aux[i])) a[k] = aux[j++]; // 右半边的当前元素小于左半边的当前元素，取右半边元素
            else a[k] = aux[i++]; // 右半边的当前元素大于等于左半边的当前元素，取左半边元素
        }

        // 验证数组是否排好序
        assert isSorted(a, lo, hi);
    }

    /**
     * 功能描述：自顶向下的归并排序，将数组a[lo..hi]排序
     *
     * @author cakin
     * @date 2021/2/27
     * @param a   待排序数组
     * @param aux 辅助数组
     * @param lo  下界
     * @param hi  上界限
     */
    private static void sort(Comparable[] a, Comparable[] aux, int lo, int hi) {
        if (hi <= lo) return;
        int mid = lo + (hi - lo) / 2;
        sort(a, aux, lo, mid); // 将左半边排序
        sort(a, aux, mid + 1, hi); // 将右半边排序
        merge(a, aux, lo, mid, hi); // 归并结果
    }

    /**
     * 功能描述：自顶向下的归并排序，对a进行排序
     *
     * @author cakin
     * @date 2021/2/27
     * @param a 待排序数组
     */
    public static void sort(Comparable[] a) {
        Comparable[] aux = new Comparable[a.length];
        sort(a, aux, 0, a.length - 1);
        assert isSorted(a);
    }


    /**
     * 功能描述：v 是否小于 w
     *
     * @param v 待比较元素第一个元素
     * @param w 待比较元素第二个元素
     * @return boolean true：小于 false：大于等于
     * @author cakin
     * @date 2021/2/26
     */
    private static boolean less(Comparable v, Comparable w) {
        return v.compareTo(w) < 0;
    }

    /**
     * 功能描述：数组的是否已排好序
     *
     * @author cakin
     * @date 2021/2/26
     * @param a 数组
     * @return boolean true：排好序 false：没排好序
     */
    private static boolean isSorted(Comparable[] a) {
        return isSorted(a, 0, a.length - 1);
    }

    /**
     * 功能描述：下标lo开始到下表hi结束的数组区间是否排好序
     *
     * @author cakin
     * @date 2021/2/26
     * @param a 数组
     * @param lo 索引的下界
     * @param hi 索引的上界
     * @return boolean true：排好序 false：没排好序
     */
    private static boolean isSorted(Comparable[] a, int lo, int hi) {
        for (int i = lo + 1; i <= hi; i++)
            if (less(a[i], a[i - 1])) return false;
        return true;
    }

    /**
     * 功能描述：打印数组
     *
     * @author cakin
     * @date 2021/2/26
     * @param a 待打印的数组
     */
    private static void show(Comparable[] a) {
        for (int i = 0; i < a.length; i++) {
            StdOut.println(a[i]);
        }
    }

    /**
     * 功能描述：自顶向下的归并排序测试
     *
     * @author cakin
     * @date 2021/2/27
     * @param
     * @return
     * @description:
     */
    public static void main(String[] args) {
        String[] a = StdIn.readAllStrings();
        Merge.sort(a);
        show(a);
    }
}

