package sort;

import common.StdIn;
import common.StdOut;

/**
 * @className: Merge
 * @description: 自底向上的归并排序
 * @date: 2021/2/27
 * @author: cakin
 */
public class MergeBU {

    public MergeBU() {
    }

    /**
     * 功能描述：原地归并
     *
     * @param a   待排序数组
     * @param aux 辅助数组
     * @param lo  下界
     * @param mid 下界和上界的中间位置
     * @param hi  上界限
     * @author cakin
     * @date 2021/2/27
     */
    private static void merge(Comparable[] a, Comparable[] aux, int lo, int mid, int hi) {
        // 初始化辅助数组
        for (int k = lo; k <= hi; k++) {
            aux[k] = a[k];
        }

        // i:左半边元素，j：右半边元素
        int i = lo, j = mid + 1;
        // 将排好序的元素放回 a 数组
        for (int k = lo; k <= hi; k++) {
            if (i > mid) a[k] = aux[j++];  // 左半边用尽，取右半边元素
            else if (j > hi) a[k] = aux[i++]; // 右半边用尽，取左半边元素
            else if (less(aux[j], aux[i])) a[k] = aux[j++]; // 右半边的当前元素小于左半边的当前元素，取右半边元素
            else a[k] = aux[i++]; // 右半边的当前元素大于等于左半边的当前元素，取左半边元素
        }
    }

    /**
     * 功能描述：自底向上的归并排序，将数组a排序
     *
     * @param a 待排序数组
     * @author cakin
     * @date 2021/2/27
     */
    public static void sort(Comparable[] a) {
        int n = a.length;
        Comparable[] aux = new Comparable[n];
        // len:子数组大小，1,2,4,8...
        for (int len = 1; len < n; len *= 2) {
            for (int lo = 0; lo < n - len; lo += len + len) {
                int mid = lo + len - 1;
                int hi = Math.min(lo + len + len - 1, n - 1);
                merge(a, aux, lo, mid, hi);
            }
        }
        assert isSorted(a);
    }

    /**
     * 功能描述：v 是否小于 w
     *
     * @param v 待比较元素第一个元素
     * @param w 待比较元素第二个元素
     * @return boolean true：小于 false：大于等于
     * @author cakin
     * @date 2021/2/26
     */
    private static boolean less(Comparable v, Comparable w) {
        return v.compareTo(w) < 0;
    }


    /**
     * 功能描述：数组的是否已排好序
     *
     * @param a 数组
     * @return boolean true：排好序 false：没排好序
     * @author cakin
     * @date 2021/2/26
     */
    private static boolean isSorted(Comparable[] a) {
        for (int i = 1; i < a.length; i++)
            if (less(a[i], a[i - 1])) return false;
        return true;
    }

    /**
     * 功能描述：打印数组
     *
     * @param a 待打印的数组
     * @author cakin
     * @date 2021/2/26
     */
    private static void show(Comparable[] a) {
        for (int i = 0; i < a.length; i++) {
            StdOut.println(a[i]);
        }
    }

    /**
     * 功能描述：自底向上的归并排序测试
     *
     * @author cakin
     * @date 2021/2/27
     * @param args 命令行
     */
    public static void main(String[] args) {
        String[] a = StdIn.readAllStrings();
        MergeBU.sort(a);
        show(a);
    }
}
