package sort;

import common.StdIn;
import common.StdOut;

import java.util.Comparator;

/**
 * @className: Selection
 * @description: 插入排序
 * @date: 2021/2/26
 * @author: cakin
 */
public class Insertion {

    public Insertion() {
    }

    /**
     * 功能描述：插入排序
     *
     * @param a 待排序数组
     * @author cakin
     * @date 2021/2/26
     */
    public static void sort(Comparable[] a) {
        int n = a.length;
        for (int i = 1; i < n; i++) {
            for (int j = i; j > 0 && less(a[j], a[j - 1]); j--) {
                exch(a, j, j - 1);
            }
            assert isSorted(a, 0, i);
        }
        assert isSorted(a);
    }

    /**
     * 功能描述：插入排序，排序范围[lo,hi)
     *
     * @param a 待排序数组
     * @param lo 排序索引下界
     * @param hi 排序索引上界
     * @author cakin
     * @date 2021/2/26
     */
    public static void sort(Comparable[] a, int lo, int hi) {
        for (int i = lo + 1; i < hi; i++) {
            for (int j = i; j > lo && less(a[j], a[j - 1]); j--) {
                exch(a, j, j - 1);
            }
        }
        assert isSorted(a, lo, hi);
    }

    /**
     * 功能描述：插入排序
     *
     * @param a          待排序数组
     * @param comparator 排序规则定义
     * @author cakin
     * @date 2021/2/26
     */
    public static void sort(Object[] a, Comparator comparator) {
        int n = a.length;
        for (int i = 1; i < n; i++) {
            for (int j = i; j > 0 && less(a[j], a[j - 1], comparator); j--) {
                exch(a, j, j - 1);
            }
            assert isSorted(a, 0, i, comparator);
        }
        assert isSorted(a, comparator);
    }

    /**
     * 功能描述：插入排序
     *
     * @param a          待排序数组
     * @param lo         排序索引下界
     * @param hi         排序索引上界
     * @param comparator 排序规则
     */
    public static void sort(Object[] a, int lo, int hi, Comparator comparator) {
        for (int i = lo + 1; i < hi; i++) {
            for (int j = i; j > lo && less(a[j], a[j - 1], comparator); j--) {
                exch(a, j, j - 1);
            }
        }
        assert isSorted(a, lo, hi, comparator);
    }

    /**
     * 功能描述：v 是否小于 w
     *
     * @param v 待比较元素第一个元素
     * @param w 待比较元素第二个元素
     * @return boolean true：小于 false：大于等于
     * @author cakin
     * @date 2021/2/26
     */
    private static boolean less(Comparable v, Comparable w) {
        return v.compareTo(w) < 0;
    }

    /**
     * 功能描述：v 是否小于 w
     *
     * @param comparator 比较规则定义
     * @param v          待比较元素第一个元素
     * @param w          待比较元素第二个元素
     * @return boolean true：小于 false：大于等于
     * @author cakin
     * @date 2021/2/26
     */
    private static boolean less(Object v, Object w, Comparator comparator) {
        return comparator.compare(v, w) < 0;
    }

    /**
     * 功能描述：交换数组a中的第i个元素和第j个元素
     *
     * @param a 待交换的数组
     * @param i 数组下标
     * @param j 数组下标
     * @author cakin
     * @date 2021/2/26
     */
    private static void exch(Object[] a, int i, int j) {
        Object swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }

    /**
     * 功能描述：数组的是否已排好序
     *
     * @author cakin
     * @date 2021/2/26
     * @param a 数组
     * @return boolean true：排好序 false：没排好序
     */
    private static boolean isSorted(Comparable[] a) {
        return isSorted(a, 0, a.length);
    }

    /**
     * 功能描述：下标lo开始到下表hi结束的数组区间是否排好序
     *
     * @author cakin
     * @date 2021/2/26
     * @param a 数组
     * @param lo 索引的下界
     * @param hi 索引的上界
     * @return boolean true：排好序 false：没排好序
     */
    private static boolean isSorted(Comparable[] a, int lo, int hi) {
        for (int i = lo + 1; i < hi; i++)
            if (less(a[i], a[i - 1])) return false;
        return true;
    }

    /**
     * 功能描述：数组的是否已排好序
     *
     * @author cakin
     * @date 2021/2/26
     * @param a 数组
     * @param comparator 排序规则接口
     * @return boolean true：排好序 false：没排好序
     */
    private static boolean isSorted(Object[] a, Comparator comparator) {
        return isSorted(a, 0, a.length, comparator);
    }

    /**
     * 功能描述：下标lo开始到下表hi结束的数组区间是否排好序
     *
     * @author cakin
     * @date 2021/2/26
     * @param a 数组
     * @param comparator 排序规则接口
     * @param lo 索引的下界
     * @param hi 索引的上界
     * @return boolean true：排好序 false：没排好序
     */
    private static boolean isSorted(Object[] a, int lo, int hi, Comparator comparator) {
        for (int i = lo + 1; i < hi; i++)
            if (less(a[i], a[i - 1], comparator)) return false;
        return true;
    }

    /**
     * 功能描述：打印数组
     *
     * @author cakin
     * @date 2021/2/26
     * @param a 待打印的数组
     */
    private static void show(Comparable[] a) {
        for (int i = 0; i < a.length; i++) {
            StdOut.println(a[i]);
        }
    }

    /**
     * 功能描述：测试插入排序
     *
     * @author cakin
     * @date 2021/2/26
     * @param args 命令行
     */
    public static void main(String[] args) {
        String[] a = StdIn.readAllStrings();
        Insertion.sort(a);
        show(a);
    }
}


