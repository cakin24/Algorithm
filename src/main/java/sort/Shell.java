package sort;

import common.StdIn;
import common.StdOut;

/**
 * @className: Shell
 * @description: 希尔排序
 * @date: 2021/2/26
 * @author: cakin
 */
public class Shell {

    public Shell() {
    }

    /**
     * 功能描述：希尔排序算法
     *
     * @param a 数组
     * @author cakin
     * @date 2021/2/27
     */
    public static void sort(Comparable[] a) {
        int n = a.length;

        // 1, 4, 13, 40, 121, 364, 1093, ...
        int h = 1;
        while (h < n / 3) h = 3 * h + 1;

        while (h >= 1) {
            // 将数组变成h有序
            for (int i = h; i < n; i++) {
                for (int j = i; j >= h && less(a[j], a[j - h]); j -= h) {
                    exch(a, j, j - h);
                }
            }
            assert isHsorted(a, h);
            h /= 3;
        }
        assert isSorted(a);
    }


    /**
     * 功能描述：v 是否小于 w
     *
     * @param v 待比较元素第一个元素
     * @param w 待比较元素第二个元素
     * @return boolean true：小于 false：大于等于
     * @author cakin
     * @date 2021/2/26
     */
    private static boolean less(Comparable v, Comparable w) {
        return v.compareTo(w) < 0;
    }

    /**
     * 功能描述：交换数组a中的第i个元素和第j个元素
     *
     * @param a 待交换的数组
     * @param i 数组下标
     * @param j 数组下标
     * @author cakin
     * @date 2021/2/26
     */
    private static void exch(Object[] a, int i, int j) {
        Object swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }


    /**
     * 功能描述：下标lo开始到下表hi结束的数组区间是否排好序
     *
     * @author cakin
     * @date 2021/2/26
     * @param a 数组
     * @return boolean true：排好序 false：没排好序
     */
    private static boolean isSorted(Comparable[] a) {
        for (int i = 1; i < a.length; i++)
            if (less(a[i], a[i - 1])) return false;
        return true;
    }

    /**
     * 功能描述：数组a是否基于步长h排序
     *
     * @author cakin
     * @date 2021/2/27
     * @param a 数组
     * @param h 步长
     */
    private static boolean isHsorted(Comparable[] a, int h) {
        for (int i = h; i < a.length; i++)
            if (less(a[i], a[i - h])) return false;
        return true;
    }

    /**
     * 功能描述：打印数组
     *
     * @author cakin
     * @date 2021/2/26
     * @param a 待打印的数组
     */
    private static void show(Comparable[] a) {
        for (int i = 0; i < a.length; i++) {
            StdOut.println(a[i]);
        }
    }

    /**
     * 功能描述：希尔排序测试
     *
     * @author cakin
     * @date 2021/2/27
     * @param args 命令行
     */
    public static void main(String[] args) {
        String[] a = StdIn.readAllStrings();
        Shell.sort(a);
        show(a);
    }

}

