package com.atguigu.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

/**
 * @className: Graph
 * @description: 图
 * @date: 2021/3/28
 * @author: cakin
 */
public class Graph {
    // 存储顶点集合
    private ArrayList<String> vertexList;
    // 存储图对应的邻结矩阵
    private int[][] edges;
    // 表示边的数目
    private int numOfEdges;
    // 表示某个结点是否被访问
    private boolean[] isVisited;

    /**
     * 功能描述：图的擦拭
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/3/28
     */
    public static void main(String[] args) {
        // 顶点
        //String Vertexs[] = {"A", "B", "C", "D", "E"};
        String Vertexs[] = {"1", "2", "3", "4", "5", "6", "7", "8"};
        // 结点的个数
        int n = Vertexs.length;
        // 创建图对象
        Graph graph = new Graph(n);
        // 循环的添加顶点
        for (String vertex : Vertexs) {
            graph.insertVertex(vertex);
        }
        // 添加边
//        graph.insertEdge(0, 1, 1); // A-B
//        graph.insertEdge(0, 2, 1); // A-C
//        graph.insertEdge(1, 2, 1); // B-C
//        graph.insertEdge(1, 3, 1); // B-D
//        graph.insertEdge(1, 4, 1); // B-E

        // 添加边的关系
        graph.insertEdge(0, 1, 1);
        graph.insertEdge(0, 2, 1);
        graph.insertEdge(1, 3, 1);
        graph.insertEdge(1, 4, 1);
        graph.insertEdge(3, 7, 1);
        graph.insertEdge(4, 7, 1);
        graph.insertEdge(2, 5, 1);
        graph.insertEdge(2, 6, 1);
        graph.insertEdge(5, 6, 1);

        // 显示邻结矩阵
        graph.showGraph();

        // dfs测试
        System.out.println("dfs:");
        graph.dfs(); // A->B->C->D->E [1->2->4->8->5->3->6->7]
		System.out.println();
        // bfs测试
        System.out.println("bfs:");
        graph.bfs(); // A->B->C->D-E [1->2->3->4->5->6->7->8]
    }

    // 构造器
    public Graph(int n) {
        // 初始化矩阵
        edges = new int[n][n];
        // 初始化 vertexList
        vertexList = new ArrayList<>(n);
        // 边的数量
        numOfEdges = 0;
    }

    /**
     * 功能描述：得到某个顶点的第一个邻接结点的下标 w
     *
     * @param index 顶点索引
     * @return 如果存在就返回对应的下标，否则返回-1
     */
    public int getFirstNeighbor(int index) {
        for (int j = 0; j < vertexList.size(); j++) {
            if (edges[index][j] > 0) {
                return j;
            }
        }
        return -1;
    }


    /**
     * 功能描述：根据前一个邻接结点的下标来获取下一个邻接结点
     *
     * @param v1 第1个顶点
     * @param v2 第2个顶点，也就是邻节点
     * @return int 邻节点的下一个邻节点
     * @author cakin
     * @date 2021/3/29
     */
    public int getNextNeighbor(int v1, int v2) {
        for (int j = v2 + 1; j < vertexList.size(); j++) {
            if (edges[v1][j] > 0) {
                return j;
            }
        }
        return -1;
    }


    /**
     * 功能描述：深度优先遍历算法
     *
     * @param isVisited 节点是否被访问数组
     * @param i         被访问节点的下标
     * @return
     * @author cakin
     * @date 2021/3/29
     * @description:
     */
    private void dfs(boolean[] isVisited, int i) {
        // 输出被访问节点
        System.out.print(getValueByIndex(i) + "->");
        // 将该结点设置为已经访问
        isVisited[i] = true;
        // 查找结点i的第一个邻接结点w
        int w = getFirstNeighbor(i);
        while (w != -1) { // 找到该邻节点
            if (!isVisited[w]) {
                // 递归进行深度优先遍历算法
                dfs(isVisited, w);
            }
            // 如果w结点已经被访问过，找下一个邻节点
            w = getNextNeighbor(i, w);
        }
    }


    /**
     * 功能描述：对 dfs 进行一个重载, 遍历所有的结点，并进行 dfs
     *
     * @author cakin
     * @date 2021/3/29
     */
    public void dfs() {
        isVisited = new boolean[vertexList.size()];
        // 遍历所有的结点，进行 dfs
        for (int i = 0; i < getNumOfVertex(); i++) {
            if (!isVisited[i]) {
                dfs(isVisited, i);
            }
        }
    }

    /**
     * 功能描述：对一个结点进行广度优先遍历
     *
     * @param isVisited 节点是否被访问数组
     * @param isVisited 节点是否被访问数组
     * @return i 被访问的节点
     * @author cakin
     * @date 2021/3/29
     */
    private void bfs(boolean[] isVisited, int i) {
        // 表示队列的头结点对应下标
        int u;
        // 邻接结点w
        int w;
        // 队列，记录结点访问的顺序
        LinkedList queue = new LinkedList();
        // 访问结点，输出结点信息
        System.out.print(getValueByIndex(i) + "=>");
        // 标记为已访问
        isVisited[i] = true;
        // 将结点加入队列
        queue.addLast(i);

        while (!queue.isEmpty()) {
            // 取出队列的头结点下标
            u = (Integer) queue.removeFirst();
            // 得到第一个邻接结点的下标 w
            w = getFirstNeighbor(u);
            while (w != -1) {//找到
                // 是否访问过
                if (!isVisited[w]) {
                    System.out.print(getValueByIndex(w) + "=>");
                    // 标记已经访问
                    isVisited[w] = true;
                    // 入队
                    queue.addLast(w);
                }
                // 以u为前驱点，找w后面的下一个邻结点，这里体现出广度优先
                w = getNextNeighbor(u, w);
            }
        }
    }

    /**
     * 功能描述：遍历所有的结点，都进行广度优先搜索
     *
     * @author cakin
     * @date 2021/3/29
     */
    public void bfs() {
        isVisited = new boolean[vertexList.size()];
        for (int i = 0; i < getNumOfVertex(); i++) {
            if (!isVisited[i]) {
                bfs(isVisited, i);
            }
        }
    }

    /**
     * 功能描述：返回结点的个数
     *
     * @author cakin
     * @date 2021/3/28
     */
    public int getNumOfVertex() {
        return vertexList.size();
    }

    /**
     * 功能描述：显示图对应的矩阵
     *
     * @author cakin
     * @date 2021/3/28
     */
    public void showGraph() {
        for (int[] link : edges) {
            System.err.println(Arrays.toString(link));
        }
    }

    /**
     * 功能描述：得到边的数目
     *
     * @return 得到边的数目
     * @author cakin
     * @date 2021/3/28
     */
    public int getNumOfEdges() {
        return numOfEdges;
    }

    //

    /**
     * 功能描述：返回结点i(下标)对应的数据
     *
     * @param i 节点下标
     * @return 节点对应的数据
     * @author cakin
     * @date 2021/3/28
     * @description: 举例如下：
     * 0->"A" 1->"B" 2->"C"
     */
    public String getValueByIndex(int i) {
        return vertexList.get(i);
    }

    /**
     * 功能描述：返回v1和v2的权值
     *
     * @param v1 第1个顶点的下标
     * @param v2 第2个顶点的下标
     * @return int 两个顶点间的权值
     * @author cakin
     * @date 2021/3/28
     */
    public int getWeight(int v1, int v2) {
        return edges[v1][v2];
    }

    /**
     * 功能描述：插入结点
     *
     * @param vertex 节点的数据
     * @author cakin
     * @date 2021/3/28
     */
    public void insertVertex(String vertex) {
        vertexList.add(vertex);
    }

    /**
     * 功能描述：添加边
     *
     * @param v1     第1个顶点对应的下标
     * @param v2     第2个顶点对应的下标
     * @param weight 表示第1个顶点和第2个顶点的权重
     */
    public void insertEdge(int v1, int v2, int weight) {
        edges[v1][v2] = weight;
        edges[v2][v1] = weight;
        numOfEdges++;
    }
}
