package com.atguigu.avl;

/**
 * @className: AVLTreeDemo
 * @description: 平衡二叉树
 * @date: 2021/3/27
 * @author: cakin
 */
public class AVLTreeDemo {
    /**
     * 功能描述：平衡二叉树测试
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/3/27
     */
    public static void main(String[] args) {
        // 测试左旋转
        // int[] arr = {4, 3, 6, 5, 7, 8};
        // 测试右旋转
        // int[] arr = {10, 12, 8, 9, 7, 6};
        // 测试双旋转
        int[] arr = {10, 11, 7, 6, 8, 9};
        // 创建一个 AVLTree对象
        AVLTree avlTree = new AVLTree();
        // 添加结点
        for (int i = 0; i < arr.length; i++) {
            avlTree.add(new Node(arr[i]));
        }

        // 遍历
        System.out.println("中序遍历");
        avlTree.infixOrder();
        System.out.println("平衡处理后：");
        System.out.println("树的高度=" + avlTree.getRoot().height());
        System.out.println("树的左子树高度=" + avlTree.getRoot().leftHeight());
        System.out.println("树的右子树高度=" + avlTree.getRoot().rightHeight());
        System.out.println("当前的根结点=" + avlTree.getRoot());
    }
}

/**
 * @className: AVLTreeDemo
 * @description: 创建AVLTree
 * @date: 2021/3/27
 * @author: cakin
 */
class AVLTree {
    // 根节点
    private Node root;

    public Node getRoot() {
        return root;
    }

    /**
     * 功能描述：查找要删除的结点
     *
     * @param value 要删除节点的值
     * @return Node 要删除的节点
     * @author cakin
     * @date 2021/3/25
     */
    public Node search(int value) {
        if (root == null) {
            return null;
        } else {
            return root.search(value);
        }
    }

    /**
     * 功能描述：要删除节点的父节点
     *
     * @param value 要删除节点的值
     * @return Node 要删除节点的父节点
     * @author cakin
     * @date 2021/3/25
     * @description:
     */
    public Node searchParent(int value) {
        if (root == null) {
            return null;
        } else {
            return root.searchParent(value);
        }
    }

    /**
     * 功能描述：返回以 node 为根结点的二叉排序树的最小结点的值
     *
     * @param node 传入的结点(当做二叉排序树的根结点)
     * @return 返回的以 node 为根结点的二叉排序树的最小结点的值
     */
    public int delRightTreeMin(Node node) {
        Node target = node;
        // 循环的查找左子节点，就会找到最小值
        while (target.left != null) {
            target = target.left;
        }
        // target就指向了最小结点
        // 删除最小结点
        delNode(target.value);
        return target.value;
    }

    /**
     * 功能描述：删除结点
     *
     * @param value 待删除节点的值
     * @author cakin
     * @date 2021/3/25
     */
    public void delNode(int value) {
        if (root == null) {
            return;
        } else {
            // 1 先去找到要删除的结点 targetNode
            Node targetNode = search(value);
            // 如果没有找到要删除的结点
            if (targetNode == null) {
                return;
            }
            // 如果发现当前这颗二叉排序树只有一个结点
            if (root.left == null && root.right == null) {
                root = null;
                return;
            }

            // 去找到 targetNode 的父结点
            Node parent = searchParent(value);
            //  如果要删除的结点是叶子结点
            if (targetNode.left == null && targetNode.right == null) {
                // 判断 targetNode 是父结点的左子结点，还是右子结点
                if (parent.left != null && parent.left.value == value) { // 是左子结点
                    parent.left = null;
                } else if (parent.right != null && parent.right.value == value) { // 是由子结点
                    parent.right = null;
                }
            } else if (targetNode.left != null && targetNode.right != null) { // 删除有两颗子树的节点
                int minVal = delRightTreeMin(targetNode.right);
                targetNode.value = minVal;

            } else { // 删除只有一颗子树的结点
                // 如果要删除的结点有左子结点
                if (targetNode.left != null) {
                    if (parent != null) {
                        // 如果 targetNode 是 parent 的左子结点
                        if (parent.left.value == value) {
                            parent.left = targetNode.left;
                        } else { // targetNode 是 parent 的右子结点
                            parent.right = targetNode.left;
                        }
                    } else {
                        root = targetNode.left;
                    }
                } else { // 如果要删除的结点有右子结点
                    if (parent != null) {
                        // 如果 targetNode 是 parent 的左子结点
                        if (parent.left.value == value) {
                            parent.left = targetNode.right;
                        } else { // 如果 targetNode 是 parent 的右子结点
                            parent.right = targetNode.right;
                        }
                    } else {
                        root = targetNode.right;
                    }
                }
            }

        }
    }

    /**
     * 功能描述：添加结点
     *
     * @param node 节点
     * @author cakin
     * @date 2021/3/22
     */
    public void add(Node node) {
        if (root == null) {
            root = node; // 如果root为空则直接让root指向node
        } else {
            root.add(node);
        }
    }

    /**
     * 功能描述：中序遍历
     *
     * @author cakin
     * @date 2021/3/22
     */
    public void infixOrder() {
        if (root != null) {
            root.infixOrder();
        } else {
            System.out.println("二叉排序树为空，不能遍历");
        }
    }
}

/**
 * @className: Node
 * @description: 节点
 * @date: 2021/3/22
 * @author: cakin
 */
class Node {
    // 节点值
    int value;
    // 左子树根节点
    Node left;
    // 右子树根节点
    Node right;

    public Node(int value) {
        this.value = value;
    }

    /**
     * 功能描述：返回左子树的高度
     *
     * @author cakin
     * @date 2021/3/27
     */
    public int leftHeight() {
        if (left == null) {
            return 0;
        }
        return left.height();
    }

    /**
     * 功能描述：返回右子树的高度
     *
     * @author cakin
     * @date 2021/3/27
     */
    public int rightHeight() {
        if (right == null) {
            return 0;
        }
        return right.height();
    }

    /**
     * 功能描述：返回以该结点为根结点的树的高度
     *
     * @author cakin
     * @date 2021/3/27
     */
    public int height() {
        return Math.max(left == null ? 0 : left.height(), right == null ? 0 : right.height()) + 1;
    }

    /**
     * 功能描述：左旋转方法
     *
     * @author cakin
     * @date 2021/3/27
     */
    private void leftRotate() {
        // 创建新的结点，值为当前根结点的值
        Node newNode = new Node(value);
        // 把新的结点的左子树设置成当前结点的左子树
        newNode.left = left;
        // 把新节点的右子树设置为当前节点右子树的左子树
        newNode.right = right.left;
        // 把当前节点的值设置为右子节点的值
        value = right.value;
        // 把当前节点的右子树设置为右子树的右子树
        right = right.right;
        // 把当前节点的左子树设置为新节点
        left = newNode;
    }

    /**
     * 功能描述：右旋转
     *
     * @author cakin
     * @date 2021/3/27
     */
    private void rightRotate() {
        Node newNode = new Node(value);
        newNode.right = right;
        newNode.left = left.right;
        value = left.value;
        left = left.left;
        right = newNode;
    }

    /**
     * 功能描述：查找要删除的结点
     *
     * @param value 希望删除的结点的值
     * @return 如果找到返回该结点，否则返回null
     */
    public Node search(int value) {
        if (value == this.value) { // 找到该结点
            return this;
        } else if (value < this.value) {// 如果查找的值小于当前结点值，向左子树递归查找
            // 如果左子结点为空
            if (this.left == null) {
                return null;
            }
            return this.left.search(value);
        } else { // 如果查找的值不小于当前结点，向右子树递归查找
            if (this.right == null) {
                return null;
            }
            return this.right.search(value);
        }
    }

    /**
     * 功能描述：查找要删除结点的父结点
     *
     * @param value 要删除的结点的值
     * @return 返回的是要删除的结点的父结点，如果没有就返回null
     */
    public Node searchParent(int value) {
        // 如果当前结点就是要删除的结点的父结点
        if ((this.left != null && this.left.value == value) ||
                (this.right != null && this.right.value == value)) {
            return this;
        } else {
            // 如果查找的值小于当前结点的值, 并且当前结点的左子结点不为空
            if (value < this.value && this.left != null) {
                return this.left.searchParent(value); // 向左子树递归查找
            } else if (value >= this.value && this.right != null) {
                return this.right.searchParent(value); // 向右子树递归查找
            } else {
                return null; // 找到父结点
            }
        }
    }

    @Override
    public String toString() {
        return "Node [value=" + value + "]";
    }

    /**
     * 功能描述：添加节点到平衡二叉树
     *
     * @param node 节点
     * @author cakin
     * @date 2021/3/22
     */
    public void add(Node node) {
        if (node == null) {
            return;
        }

        // 传入的结点的值小于当前子树的根结点的值
        if (node.value < this.value) {
            // 当前结点左子树根结点为null
            if (this.left == null) {
                this.left = node;
            } else {
                // 递归的向左子树添加
                this.left.add(node);
            }
        } else { // 传入的结点的值大于当前子树的根结点的值
            if (this.right == null) {
                this.right = node;
            } else {
                // 递归的向右子树添加
                this.right.add(node);
            }
        }

        // 当添加完一个结点后，如果(右子树的高度-左子树的高度) > 1 , 进行左旋转
        if (rightHeight() - leftHeight() > 1) {
            // 左旋转
            // leftRotate();
            // 如果它的右子树的左子树的高度大于它的右子树的右子树的高度
            if (right != null && right.leftHeight() > right.rightHeight()) {
                // 先对右子结点进行右旋转
                right.rightRotate();
                // 然后再对当前结点进行左旋转
                leftRotate();
            } else {
                // 直接进行左旋转
                leftRotate();
            }
            return;
        }

        // 当添加完一个结点后，如果(左子树的高度 - 右子树的高度) > 1, 进行左旋转
        if (leftHeight() - rightHeight() > 1) {
            // rightRotate();
            // 如果它的左子树的右子树高度大于它的左子树的高度
            if (left != null && left.rightHeight() > left.leftHeight()) {
                // 先对当前结点的左子结点进行左旋转
                left.leftRotate();
                // 再对当前结点进行右旋转
                rightRotate();
            } else {
                // 直接进行右旋转
                rightRotate();
            }
        }
    }

    /**
     * 功能描述：中序遍历
     *
     * @author cakin
     * @date 2021/3/22
     */
    public void infixOrder() {
        if (this.left != null) {
            this.left.infixOrder();
        }
        System.out.println(this);
        if (this.right != null) {
            this.right.infixOrder();
        }
    }
}
