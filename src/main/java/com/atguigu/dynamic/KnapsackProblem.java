package com.atguigu.dynamic;

/**
 * @className: KnapsackProblem
 * @description: 背包问题
 * @date: 2021/3/30
 * @author: cakin
 */
public class KnapsackProblem {
    /**
     * 功能描述：背包问题测试
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/3/30
     * @description 公式
     * v[i][0]=v[0][j]=0;
     * 当w[i] > j 时：v[i][j]=v[i-1][j]
     * 当j >=w[i]时：v[i][j]=max{v[i-1][j], v[i]+v[i-1][j-w[i]]
     */
    public static void main(String[] args) {
        // 物品的重量分别是1磅、4磅、3磅
        int[] w = {1, 4, 3};
        // 物品的价值
        int[] val = {1500, 3000, 2000};
        // 背包的容量
        int m = 4;
        // 物品的个数，这里是3个
        int n = val.length;
        // v[n][m] 表示在前 n 个物品中能够装入容量为 m 的背包中的最大价值
        int[][] v = new int[n + 1][m + 1];
        // 记录放入商品的情况
        int[][] path = new int[n + 1][m + 1];

        // 初始化第一行和第一列,在本程序中，可以不去处理，因为默认就是0
        for (int i = 0; i < v.length; i++) {
            v[i][0] = 0; // 将第一列设置为0
        }
        for (int i = 0; i < v[0].length; i++) {
            v[0][i] = 0; // 将第一行设置0
        }

        // 根据公式来动态规划处理，从上到下，从左到右处理
        for (int i = 1; i < v.length; i++) { // 不处理 i=0 行 i是从1开始
            for (int j = 1; j < v[0].length; j++) { // 不处理 j=0 列，j是从1开始
                // 公式
                if (w[i - 1] > j) { // 因为我们程序 i 是从1开始的，因此原来公式中的 w[i] 修改成 w[i-1]
                    v[i][j] = v[i - 1][j];
                } else {
                    // i 从1开始的， 公式需要调整
                    // 原始的公式：v[i][j] = max{v[i-1][j], v[i]+v[i-1][j-w[i]]
                    // 调整后公式：v[i][j] = Math.max(v[i-1][j], val[i-1]+v[i-1][j-w[i- 1]]);
                    // 为了记录商品存放到背包的情况，我们不能直接的使用上面的公式，需要使用if-else来体现公式，将较大的值放到v[i][j]中
                    if (v[i - 1][j] < val[i - 1] + v[i - 1][j - w[i - 1]]) {
                        v[i][j] = val[i - 1] + v[i - 1][j - w[i - 1]];
                        // 把当前的情况记录到path
                        path[i][j] = 1;
                    } else {
                        // 等于上一层单元格的值
                        v[i][j] = v[i - 1][j];
                    }
                }
            }
        }

        // 输出v
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {
                System.out.print(v[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("============================");
        // 输出最后我们是放入的哪些商品
        // 遍历path, 输出会把所有的放入情况都得到, 其实我们只需要最后的放入
//		for(int i = 0; i < path.length; i++) {
//			for(int j=0; j < path[i].length; j++) {
//				if(path[i][j] == 1) {
//					System.out.printf("第%d个商品放入到背包\n", i);
//				}
//			}
//		}

        int i = path.length - 1; // 行的最大下标
        int j = path[0].length - 1;  // 列的最大下标
        while (i > 0 && j > 0) { // 从 path 的最后开始找
            if (path[i][j] == 1) {
                System.out.printf("第%d个商品放入到背包\n", i);
                j -= w[i - 1]; //w[i-1]
            }
            i--;
        }
    }
}
