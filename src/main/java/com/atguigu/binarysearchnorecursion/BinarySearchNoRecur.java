package com.atguigu.binarysearchnorecursion;

/**
 * @className: BinarySearchNoRecur
 * @description: 使用非递归的方式完成二分法
 * @date: 2021/3/29
 * @author: cakin
 */
public class BinarySearchNoRecur {
    /**
     * 功能描述：使用非递归的方式完成二分法测试
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/3/29
     */
    public static void main(String[] args) {
        // 测试
        int[] arr = {1, 3, 8, 10, 11, 67, 100};
        int index = binarySearch(arr, 11);
        System.out.println("index=" + index);//
    }

    /**
     * 功能描述：二分查找的非递归实现
     *
     * @param arr    待查找的数组, arr是升序排序
     * @param target 需要查找的数
     * @return 返回对应下标，-1表示没有找到
     */
    public static int binarySearch(int[] arr, int target) {
        int left = 0;
        int right = arr.length - 1;
        while (left <= right) { // 要继续查找
            int mid = (left + right) / 2;
            if (arr[mid] == target) {
                return mid;
            } else if (arr[mid] > target) {
                right = mid - 1; // 需要向左边查找
            } else {
                left = mid + 1; // 需要向右边查找
            }
        }
        return -1;
    }
}
