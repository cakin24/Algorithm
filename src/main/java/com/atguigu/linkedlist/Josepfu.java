package com.atguigu.linkedlist;

/**
 * @className: Josepfu
 * @description: 约瑟夫问题
 * @date: 2021/3/5
 * @author: cakin
 */
public class Josepfu {
    /**
     * 功能描述：约瑟夫问题测试
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/3/5
     */
    public static void main(String[] args) {
        // 构建环形链表
        CircleSingleLinkedList circleSingleLinkedList = new CircleSingleLinkedList();
        circleSingleLinkedList.addBoy(63); // 加入63个小孩节点
        // 遍历环形列表
        circleSingleLinkedList.showBoy();
        // 测试小孩出圈
        circleSingleLinkedList.countBoy(10, 20, 63);
    }
}

/**
 * @className: CircleSingleLinkedList
 * @description: 创建一个环形的单向链表
 * @date: 2021/3/5
 * @author: cakin
 */
class CircleSingleLinkedList {
    // 创建一个first节点,当前没有编号
    private Boy first = null;

    // 添加小孩节点，构建成一个环形的链表
    public void addBoy(int nums) {
        // 数据校验
        if (nums < 1) {
            System.out.println("nums的值不正确");
            return;
        }
        // 辅助指针，帮助构建环形链表
        Boy curBoy = null;
        // 创建环形链表
        for (int i = 1; i <= nums; i++) {
            // 根据编号，创建小孩节点
            Boy boy = new Boy(i);
            // 如果是第一个小孩
            if (i == 1) {
                first = boy;
                first.setNext(first); // 构成环
                curBoy = first; // 让curBoy指向第一个小孩
            } else {
                curBoy.setNext(boy);
                boy.setNext(first);
                curBoy = boy;
            }
        }
    }

    // 遍历当前的环形链表
    public void showBoy() {
        // 判断链表是否为空
        if (first == null) {
            System.out.println("没有任何小孩~~");
            return;
        }
        // 辅助指针帮助完成遍历
        Boy curBoy = first;
        while (true) {
            System.out.printf("小孩的编号 %d \n", curBoy.getNo());
            if (curBoy.getNext() == first) { // 说明已经遍历完毕
                break;
            }
            curBoy = curBoy.getNext(); // curBoy后移
        }
    }


    /**
     * 功能描述：根据用户的输入，计算出小孩出圈的顺序
     *
     * @param startNo  表示从第几个小孩开始数数
     * @param countNum 表示数几下
     * @param nums     表示最初有多少小孩在圈中
     * @author cakin
     * @date 2021/3/5
     */
    public void countBoy(int startNo, int countNum, int nums) {
        // 数据进行校验
        if (first == null || startNo < 1 || startNo > nums) {
            System.out.println("参数输入有误，请重新输入");
            return;
        }
        // 辅助指针,帮助完成小孩出圈
        Boy helper = first;
        // 事先应该指向环形链表的最后这个节点
        while (true) {
            // 说明helper指向最后小孩节点
            if (helper.getNext() == first) {
                break;
            }
            helper = helper.getNext();
        }
        // 小孩报数前，先让 first 和  helper 移动 k - 1次
        for (int j = 0; j < startNo - 1; j++) {
            first = first.getNext();
            helper = helper.getNext();
        }
        while (true) {
            // 圈中只有一个节点
            if (helper == first) {
                break;
            }
            //当小孩报数时,让 first 和 helper 指针同时 的移动 countNum - 1 次, 然后出圈
            for (int j = 0; j < countNum - 1; j++) {
                first = first.getNext();
                helper = helper.getNext();
            }
            // first指向的节点，就是要出圈的小孩节点
            System.out.printf("小孩%d出圈\n", first.getNo());
            // 将first指向的小孩节点出圈
            first = first.getNext();
            helper.setNext(first);
        }
        System.out.printf("最后留在圈中的小孩编号%d \n", first.getNo());
    }
}


/**
 * @className: Boy
 * @description: 小孩节点
 * @date: 2021/3/5
 * @author: cakin
 */
class Boy {
    private int no; // 编号
    private Boy next; // 指向下一个节点,默认null

    public Boy(int no) {
        this.no = no;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public Boy getNext() {
        return next;
    }

    public void setNext(Boy next) {
        this.next = next;
    }

}
