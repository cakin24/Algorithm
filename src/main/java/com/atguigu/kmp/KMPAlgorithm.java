package com.atguigu.kmp;

import java.util.Arrays;

/**
 * @className: KMPAlgorithm
 * @description: KMP算法
 * @date: 2021/3/30
 * @author: cakin
 */
public class KMPAlgorithm {
    /**
     * 功能描述：KMP算法测试
     *
     * @param args 命令行
     * @author 贝医
     * @date 2021/3/30
     */
    public static void main(String[] args) {
        String str1 = "BBC ABCDAB ABCDABCDABDE";
        String str2 = "ABCDABD";
        // String str2 = "BBC";
        // 获取字符串的部分匹配表
        int[] next = kmpNext("ABCDABD"); // [0, 0, 0, 0, 1, 2, 0]
        System.out.println("next=" + Arrays.toString(next));
        // kmp算法搜索
        int index = kmpSearch(str1, str2, next);
        System.out.println("index=" + index); // 15
    }

    /**
     * 功能描述：kmp搜索算法
     *
     * @param str1 源字符串
     * @param str2 子串
     * @param next 部分匹配表, 是子串对应的部分匹配表
     * @return 如果是-1就是没有匹配到，否则返回第一个匹配的位置
     */
    public static int kmpSearch(String str1, String str2, int[] next) {
        // i:源字符串指针
        // j:子串指针
        for (int i = 0, j = 0; i < str1.length(); i++) {
            // 需要处理 str1.charAt(i) ！= str2.charAt(j), 去调整j的大小
            while (j > 0 && str1.charAt(i) != str2.charAt(j)) {
                j = next[j - 1];
            }
            // 源字符串和子串相等，子串指针后移
            if (str1.charAt(i) == str2.charAt(j)) {
                j++;
            }
            // 子串所有字符都匹配，就找到了，对应源字符串的位置就是将此时的i前移（j-1）个位置。
            if (j == str2.length()) {
                return i - j + 1;
            }
        }
        return -1;
    }

    /**
     * 功能描述：获取到一个字符串(子串) 的部分匹配值表
     *
     * @param dest 字符串
     * @return int[] 字符串的部分匹配表
     * @author cakin
     * @date 2021/3/30
     */
    public static int[] kmpNext(String dest) {
        // next 数组保存部分匹配值
        int[] next = new int[dest.length()];
        next[0] = 0; // 如果字符串是长度为1，部分匹配值就是0
        for (int i = 1, j = 0; i < dest.length(); i++) {
            // 当dest.charAt(i) != dest.charAt(j) ，需要从next[j-1]获取新的j
            // 直到发现有  dest.charAt(i) == dest.charAt(j)成立才退出
            // 这是kmp算法的核心点
            while (j > 0 && dest.charAt(i) != dest.charAt(j)) {
                j = next[j - 1];
            }

            // 当dest.charAt(i) == dest.charAt(j) 满足时，部分匹配值就是+1
            if (dest.charAt(i) == dest.charAt(j)) {
                j++;
            }
            next[i] = j;
        }
        return next;
    }
}
