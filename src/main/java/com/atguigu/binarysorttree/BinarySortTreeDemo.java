package com.atguigu.binarysorttree;

/**
 * @className: BinarySortTreeDemo
 * @description: 二叉排序树
 * @date: 2021/3/22
 * @author: cakin
 */
public class BinarySortTreeDemo {
    /**
     * 功能描述：二叉排序树测试
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/3/22
     */
    public static void main(String[] args) {
        int[] arr = {7, 3, 10, 12, 5, 1, 9, 2};
        BinarySortTree binarySortTree = new BinarySortTree();
        // 循环的添加结点到二叉排序树
        for (int i = 0; i < arr.length; i++) {
            binarySortTree.add(new Node(arr[i]));
        }
        // 中序遍历二叉排序树
        System.out.println("中序遍历二叉排序树：");
        binarySortTree.infixOrder(); // 1, 3, 5, 7, 9, 10, 12
        // 删除节点测试
        binarySortTree.delNode(12);
        binarySortTree.delNode(5);
        binarySortTree.delNode(10);
        binarySortTree.delNode(2);
        binarySortTree.delNode(3);
        binarySortTree.delNode(9);
        binarySortTree.delNode(1);
        binarySortTree.delNode(7);

        System.out.println("root=" + binarySortTree.getRoot());
        System.out.println("删除结点后");
        binarySortTree.infixOrder();
    }
}


/**
 * @className: BinarySortTreeDemo
 * @description: 二叉排序树
 * @date: 2021/3/22
 * @author: cakin
 */
class BinarySortTree {
    // 根节点
    private Node root;

    public Node getRoot() {
        return root;
    }

    /**
     * 功能描述：查找要删除的结点
     *
     * @param value 要删除节点的值
     * @return Node 要删除的节点
     * @author 贝医
     * @date 2021/3/25
     */
    public Node search(int value) {
        if (root == null) {
            return null;
        } else {
            return root.search(value);
        }
    }

    /**
     * 功能描述：要删除节点的父节点
     *
     * @param value 要删除节点的值
     * @return Node 要删除节点的父节点
     * @author 贝医
     * @date 2021/3/25
     * @description:
     */
    public Node searchParent(int value) {
        if (root == null) {
            return null;
        } else {
            return root.searchParent(value);
        }
    }


    /**
     * 功能描述：返回以 node 为根结点的二叉排序树的最小结点的值
     *
     * @param node 传入的结点(当做二叉排序树的根结点)
     * @return 返回的以 node 为根结点的二叉排序树的最小结点的值
     */
    public int delRightTreeMin(Node node) {
        Node target = node;
        // 循环的查找左子节点，就会找到最小值
        while (target.left != null) {
            target = target.left;
        }
        // target就指向了最小结点
        // 删除最小结点
        delNode(target.value);
        return target.value;
    }


    /**
     * 功能描述：删除结点
     *
     * @param value 待删除节点的值
     * @author 贝医
     * @date 2021/3/25
     */
    public void delNode(int value) {
        if (root == null) {
            return;
        } else {
            // 1 先去找到要删除的结点 targetNode
            Node targetNode = search(value);
            // 如果没有找到要删除的结点
            if (targetNode == null) {
                return;
            }
            // 如果发现当前这颗二叉排序树只有一个结点
            if (root.left == null && root.right == null) {
                root = null;
                return;
            }

            // 去找到 targetNode 的父结点
            Node parent = searchParent(value);
            // 如果要删除的结点是叶子结点
            if (targetNode.left == null && targetNode.right == null) {
                // 判断 targetNode 是父结点的左子结点，还是右子结点
                if (parent.left != null && parent.left.value == value) { // 是左子结点
                    parent.left = null;
                } else if (parent.right != null && parent.right.value == value) { // 是由子结点
                    parent.right = null;
                }
            } else if (targetNode.left != null && targetNode.right != null) { // 删除有两颗子树的节点
                int minVal = delRightTreeMin(targetNode.right);
                targetNode.value = minVal;
            } else { // 删除只有一颗子树的结点
                // 如果要删除的结点有左子结点
                if (targetNode.left != null) {
                    if (parent != null) {
                        // 如果 targetNode 是 parent 的左子结点
                        if (parent.left.value == value) {
                            parent.left = targetNode.left;
                        } else { // targetNode 是 parent 的右子结点
                            parent.right = targetNode.left;
                        }
                    } else {
                        root = targetNode.left;
                    }
                } else { // 如果要删除的结点有右子结点
                    if (parent != null) {
                        // 如果 targetNode 是 parent 的左子结点
                        if (parent.left.value == value) {
                            parent.left = targetNode.right;
                        } else { // 如果 targetNode 是 parent 的右子结点
                            parent.right = targetNode.right;
                        }
                    } else {
                        root = targetNode.right;
                    }
                }
            }
        }
    }

    /**
     * 功能描述：添加结点
     *
     * @param node 节点
     * @author cakin
     * @date 2021/3/22
     */
    public void add(Node node) {
        if (root == null) {
            root = node; // 如果root为空则直接让root指向node
        } else {
            root.add(node);
        }
    }

    /**
     * 功能描述：中序遍历
     *
     * @author cakin
     * @date 2021/3/22
     */
    public void infixOrder() {
        if (root != null) {
            root.infixOrder();
        } else {
            System.out.println("二叉排序树为空，不能遍历");
        }
    }
}

/**
 * @className: Node
 * @description: 节点
 * @date: 2021/3/22
 * @author: cakin
 */
class Node {
    // 节点值
    int value;
    // 左子树根节点
    Node left;
    // 右子树根节点
    Node right;

    public Node(int value) {
        this.value = value;
    }

    /**
     * 功能描述：查找要删除的结点
     *
     * @param value 希望删除的结点的值
     * @return 如果找到返回该结点，否则返回null
     */
    public Node search(int value) {
        if (value == this.value) { // 找到该结点
            return this;
        } else if (value < this.value) {// 如果查找的值小于当前结点值，向左子树递归查找
            // 如果左子结点为空
            if (this.left == null) {
                return null;
            }
            return this.left.search(value);
        } else { // 如果查找的值不小于当前结点，向右子树递归查找
            if (this.right == null) {
                return null;
            }
            return this.right.search(value);
        }
    }

    /**
     * 功能描述：查找要删除结点的父结点
     *
     * @param value 要删除的结点的值
     * @return 返回的是要删除的结点的父结点，如果没有就返回null
     */
    public Node searchParent(int value) {
        // 如果当前结点就是要删除的结点的父结点
        if ((this.left != null && this.left.value == value) ||
                (this.right != null && this.right.value == value)) {
            return this;
        } else {
            // 如果查找的值小于当前结点的值, 并且当前结点的左子结点不为空
            if (value < this.value && this.left != null) {
                return this.left.searchParent(value); // 向左子树递归查找
            } else if (value >= this.value && this.right != null) {
                return this.right.searchParent(value); // 向右子树递归查找
            } else {
                return null; // 找到父结点
            }
        }
    }

    @Override
    public String toString() {
        return "Node [value=" + value + "]";
    }

    /**
     * 功能描述：添加节点到二叉排序树
     *
     * @param node 节点
     * @author cakin
     * @date 2021/3/22
     */
    public void add(Node node) {
        if (node == null) {
            return;
        }

        // 传入的结点的值小于当前子树的根结点的值
        if (node.value < this.value) {
            // 当前结点左子树根结点为null
            if (this.left == null) {
                this.left = node;
            } else {
                // 递归的向左子树添加
                this.left.add(node);
            }
        } else { // 传入的结点的值大于当前子树的根结点的值
            if (this.right == null) {
                this.right = node;
            } else {
                // 递归的向右子树添加
                this.right.add(node);
            }
        }
    }

    /**
     * 功能描述：中序遍历
     *
     * @author cakin
     * @date 2021/3/22
     */
    public void infixOrder() {
        if (this.left != null) {
            this.left.infixOrder();
        }
        System.out.println(this);
        if (this.right != null) {
            this.right.infixOrder();
        }
    }
}
