package com.atguigu.recursion;

/**
 * @className: RecursionTest
 * @description: 递归的简单测试
 * @date: 2021/3/7
 * @author: cakin
 */
public class RecursionTest {
	/**
	 * 功能描述：递归调用机制测试
	 *
	 * @author cakin
	 * @date 2021/3/7
	 * @param args 命令行
	 */
    public static void main(String[] args) {
		System.out.println("=============打印问题================");
        test(4);
		System.out.println("=============阶乘问题================");
        int res = factorial(3);
        System.out.println("res=" + res);
    }

	/**
	 * 功能描述：打印问题
	 *
	 * @author cakin
	 * @date 2021/3/7
	 * @param n 打印次数
	 */
    public static void test(int n) {
        if (n > 2) {
            test(n - 1);
        }
        System.out.println("n=" + n);
    }

	/**
	 * 功能描述：阶乘问题
	 *
	 * @author cakin
	 * @date 2021/3/7
	 * @param n 阶乘数
	 * @return n 计算结果
	 */
    public static int factorial(int n) {
        if (n == 1) {
            return 1;
        } else {
            return factorial(n - 1) * n; // 1 * 2 * 3
        }
    }
}
