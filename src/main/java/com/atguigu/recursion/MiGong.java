package com.atguigu.recursion;

/**
 * @className: MiGong
 * @description: 用递归求解迷宫问题
 * @date: 2021/3/7
 * @author: cakin
 */
public class MiGong {
    /**
     * 功能描述：迷宫问题测试
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/3/7
     */
    public static void main(String[] args) {
        // 二维数组，模拟迷宫地图
        int[][] map = new int[8][7];
        // 用1表示墙
        // 上下全部置为1
        for (int i = 0; i < 7; i++) {
            map[0][i] = 1;
            map[7][i] = 1;
        }

        // 左右全部置为1
        for (int i = 0; i < 8; i++) {
            map[i][0] = 1;
            map[i][6] = 1;
        }
        // 设置其他墙, 1 表示
        map[3][1] = 1;
        map[3][2] = 1;
        map[3][4] = 1;
        map[3][5] = 1;
        map[3][6] = 1;
        // 下面两条语句测试回溯过程
        // map[1][2] = 1;
        // map[2][2] = 1;

        // 二维数组深拷贝，使得map2地图和map地图一样
        int[][] map2 = new int[8][7];
        for (int i = 0; i < map.length; i++) {
            map2[i] = map[i].clone();
        }

        // 输出地图map
        System.out.println("地图的情况");
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 7; j++) {
                System.out.print(map[i][j] + " ");
            }
            System.out.println();
        }

        // 使用递归寻找路径，逆时针策略
        setWay(map, 1, 1);
        System.out.println("====逆时针策略求解迷宫路径============");
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 7; j++) {
                System.out.print(map[i][j] + " ");
            }
            System.out.println();
        }
        // 使用递归寻找路径，顺时针策略
        setWay2(map2, 1, 1);
        System.out.println("====顺时针策略求解迷宫路径============");
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 7; j++) {
                System.out.print(map2[i][j] + " ");
            }
            System.out.println();
        }
    }

    /**
     * 功能描述：逆时针策略求解迷宫路径，在地图上走一步
     *
     * @param map 表示地图
     * @param i   从哪个位置开始找
     * @param j
     * @return 如果找到通路，就返回true, 否则返回false
     * @author cakin
     * @date 2021/3/7
     * @description: 算法说明：
     * 1 map 表示地图
     * 2 i,j 表示从地图的哪个位置开始出发，如果i=1，j=1，表示从(1,1)这个位置出发
     * 3 如果小球能到 map[6][5] 位置，则说明小球走出迷宫
     * 4 约定：当 map[i][j] 取值有以下几种：
     * 当为 0：表示该点没有走过
     * 当为 1：表示墙
     * 当为 2：表示通路可以走
     * 当为 3：表示该点已经走过，但是走不通
     * 5 在走迷宫时，需要确定一个策略：例如 下->右->上->左（逆时针） , 如果该点走不通，再回溯
     */
    public static boolean setWay(int[][] map, int i, int j) {
        if (map[6][5] == 2) { // 通路已经找到
            return true;
        } else {
            if (map[i][j] == 0) { // 当前这个点还没有走过
                // 按照策略 下->右->上->左  走
                map[i][j] = 2; // 假定该点是可以走通，将该点设置为2
                if (setWay(map, i + 1, j)) { // 向下走
                    return true;
                } else if (setWay(map, i, j + 1)) { // 向右走
                    return true;
                } else if (setWay(map, i - 1, j)) { //向上
                    return true;
                } else if (setWay(map, i, j - 1)) { // 向左走
                    return true;
                } else {
                    // 其他情况，说明该点走不通，是死路，将该点设置为3
                    map[i][j] = 3;
                    return false;
                }
            } else { // 如果map[i][j] != 0 , 可能是 1， 2， 3，这些情况都是死路，走不通
                return false;
            }
        }
    }

    /**
     * 功能描述：逆时针策略求解迷宫路径，在地图上走一步
     *
     * @param map 表示地图
     * @param i   从哪个位置开始找
     * @param j
     * @return 如果找到通路，就返回true, 否则返回false
     * @author cakin
     * @date 2021/3/7
     * @description: 算法说明：
     * 1 map 表示地图
     * 2 i,j 表示从地图的哪个位置开始出发，如果i=1，j=1，表示从(1,1)这个位置出发
     * 3 如果小球能到 map[6][5] 位置，则说明小球走出迷宫
     * 4 约定：当 map[i][j] 取值有以下几种：
     * 当为 0：表示该点没有走过
     * 当为 1：表示墙
     * 当为 2：表示通路可以走
     * 当为 3：表示该点已经走过，但是走不通
     * 5 在走迷宫时，需要确定一个策略：例如 上->右->下->左（顺时针） , 如果该点走不通，再回溯
     */
    public static boolean setWay2(int[][] map, int i, int j) {
        if (map[6][5] == 2) { // 通路已经找到
            return true;
        } else {
            if (map[i][j] == 0) { // 当前这个点还没有走过
                // 按照策略 上->右->下->左 走
                map[i][j] = 2; // 假定该点是可以走通，将该点设置为2
                if (setWay2(map, i - 1, j)) { // 向上走
                    return true;
                } else if (setWay2(map, i, j + 1)) { // 向右走
                    return true;
                } else if (setWay2(map, i + 1, j)) { // 向下走
                    return true;
                } else if (setWay2(map, i, j - 1)) { // 向左走
                    return true;
                } else {
                    // 其他情况，说明该点走不通，是死路，将该点设置为3
                    map[i][j] = 3;
                    return false;
                }
            } else { // 如果map[i][j] != 0 , 可能是 1， 2， 3，这些情况都是死路，走不通
                return false;
            }
        }
    }
}
