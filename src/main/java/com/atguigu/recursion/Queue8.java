package com.atguigu.recursion;

/**
 * @className: Queue8
 * @description: 八皇后问题
 * @date: 2021/3/7
 * @author: cakin
 */
public class Queue8 {
    // 表示共有多少个皇后
    int max = 8;
    // 保存皇后放置的位置,比如 arr = {0 , 4, 7, 5, 2, 6, 1, 3} 就是一种摆法方法
    int[] array = new int[max];
    // 解法个数
    static int count = 0;
    // 判断冲突次数
    static int judgeCount = 0;

    public static void main(String[] args) {
        Queue8 queue8 = new Queue8();
        queue8.check(0);
        System.out.printf("一共有%d解法，", count); // 92种
        System.out.printf("一共判断冲突的次数%d次", judgeCount); // 15720次
    }

    /**
     * 功能描述：放置第n个皇后
     *
     * @param n 放置第几个皇后
     * @author cakin
     * @date 2021/3/7
     * @description: 特别注意： check 是 每一次递归时，进入到check中都有  for(int i = 0; i < max; i++)，因此会有回溯
     */
    private void check(int n) {
        if (n == max) {  // 当 n = 8 时, 其实8个皇后已经放好了
            print();
            return;
        }
        // 依次放入皇后，并判断是否冲突，i++表示第n个皇后的第i列已经放过了，再放第i+1个列，也就是说当前行的所有列都要探一遍
        for (int i = 0; i < max; i++) {
            // 先把当前这个皇后n, 放到该行的第1列，然后依次放到第2、3、4...列
            array[n] = i;
            // 判断当放置第n个皇后到i列时，是否冲突
            if (judge(n)) { // 如果不冲突，就接着放第n+1个皇后，开始递归
                check(n + 1);
            }
        }
    }

    /**
     * 功能描述：当我们放置第n个皇后时, 就去检测该皇后是否和前面已经摆放的皇后冲突
     *
     * @param n 表示第n个皇后
     * @return boolean：false：表示冲突 true：表示不冲突
     * @author cakin
     * @date 2021/3/7
     */
    private boolean judge(int n) {
        judgeCount++;
        for (int i = 0; i < n; i++) {
            // 1 array[i] == array[n]，用于判断第n个皇后是否和第i个皇后在同一列。
            // 2 Math.abs(n-i) == Math.abs(array[n] - array[i])，用于判断第n个皇后和第i皇后是否在同一斜线。原理：两个点的横坐标之差和纵坐标之差相等，则在同一斜线。
            // 3 判断是否在同一行, 没有必要，n 其实就是横坐标的位置，肯定不一样。
            if (array[i] == array[n] || Math.abs(n - i) == Math.abs(array[n] - array[i])) {
                return false;
            }
        }
        return true;
    }

    /**
     * 功能描述：皇后摆放的位置输出
     *
     * @author cakin
     * @date 2021/3/7
     */
    private void print() {
        count++;
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }
}
