package com.atguigu.prim;

import java.util.Arrays;

/**
 * @className: PrimAlgorithm
 * @description: 普里姆算法
 * @date: 2021/3/31
 * @author: cakin
 */
public class PrimAlgorithm {
    /**
     * 功能描述：普里姆算法测试
     *
     * @param args 命令行
     * @return
     * @author cakin
     * @date 2021/3/31
     */
    public static void main(String[] args) {
        // 图的顶点
        char[] data = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G'};
        // 顶点的个数
        int verxs = data.length;
        // 邻接矩阵的关系使用二维数组表示,10000这个大数，表示两个点不联通
        int[][] weight = new int[][]{
                {10000, 5, 7, 10000, 10000, 10000, 2},
                {5, 10000, 10000, 9, 10000, 10000, 3},
                {7, 10000, 10000, 10000, 8, 10000, 10000},
                {10000, 9, 10000, 10000, 10000, 4, 10000},
                {10000, 10000, 8, 10000, 10000, 5, 4},
                {10000, 10000, 10000, 4, 5, 10000, 6},
                {2, 3, 10000, 10000, 4, 6, 10000},};

        // 创建 MGraph 对象
        MGraph graph = new MGraph(verxs);
        // 创建 MinTree 对象
        MinTree minTree = new MinTree();
        minTree.createGraph(graph, verxs, data, weight);
        // 输出图
        minTree.showGraph(graph);
        // 普利姆算法
        minTree.prim(graph, 1);//
    }

}

/**
 * @className: PrimAlgorithm
 * @description: 最小生成树
 * @date: 2021/3/31
 * @author: cakin
 */
class MinTree {
    /**
     * 功能描述：图的邻接矩阵
     *
     * @param graph  图对象
     * @param verxs  图对应的顶点个数
     * @param data   图的各个顶点的值
     * @param weight 图的邻接矩阵
     */
    public void createGraph(MGraph graph, int verxs, char data[], int[][] weight) {
        int i, j;
        for (i = 0; i < verxs; i++) {//顶点
            graph.data[i] = data[i];
            for (j = 0; j < verxs; j++) {
                graph.weight[i][j] = weight[i][j];
            }
        }
    }

    /**
     * 功能描述：显示图的邻接矩阵
     *
     * @param graph 图
     * @author 贝医
     * @date 2021/3/31
     */
    public void showGraph(MGraph graph) {
        for (int[] link : graph.weight) {
            System.out.println(Arrays.toString(link));
        }
    }

    /**
     * 功能描述：prim算法，得到最小生成树
     *
     * @param graph 图
     * @param v     表示从图的第几个顶点开始生成'A'->0 'B'->1...
     */
    public void prim(MGraph graph, int v) {
        // 标记结点(顶点)是否被访问过
        int visited[] = new int[graph.verxs];
        //visited[] 默认元素的值都是0, 表示没有访问过
//		for(int i =0; i <graph.verxs; i++) {
//			visited[i] = 0;
//		}

        // 把当前这个结点标记为已访问
        visited[v] = 1;
        // h1 和 h2 记录两个顶点的下标
        int h1 = -1;
        int h2 = -1;
        int minWeight = 10000; // 将 minWeight 初始成一个大数，后面在遍历过程中，会被替换
        for (int k = 1; k < graph.verxs; k++) { // 因为有 graph.verxs 个顶点，普利姆算法结束后，有 graph.verxs-1 边
            // 这个是确定每一次生成的子图 ，和哪个结点的距离最近
            for (int i = 0; i < graph.verxs; i++) { // i结点表示被访问过的结点
                for (int j = 0; j < graph.verxs; j++) { // j结点表示还没有访问过的结点
                    // 寻找已经访问过的结点和未访问过的结点间的权值最小的边
                    if (visited[i] == 1 && visited[j] == 0 && graph.weight[i][j] < minWeight) {
                        // 替换minWeight
                        minWeight = graph.weight[i][j];
                        h1 = i;
                        h2 = j;
                    }
                }
            }
            // 找到一条边是最小
            System.out.println("边<" + graph.data[h1] + "," + graph.data[h2] + "> 权值:" + minWeight);
            // 将当前这个结点标记为已经访问
            visited[h2] = 1;
            // minWeight 重新设置为最大值 10000
            minWeight = 10000;
        }
    }
}

/**
 * @className: PrimAlgorithm
 * @description: 图
 * @date: 2021/3/31
 * @author: cakin
 */
class MGraph {
    // 表示图的顶点个数
    int verxs;
    // 存储结点数据
    char[] data;
    // 存放边，也就是邻接矩阵
    int[][] weight;

    public MGraph(int verxs) {
        this.verxs = verxs;
        data = new char[verxs];
        weight = new int[verxs][verxs];
    }
}

