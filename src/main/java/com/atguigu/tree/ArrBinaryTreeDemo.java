package com.atguigu.tree;

/**
 * @className: ArrBinaryTreeDemo
 * @description: 顺序存储二叉树
 * @date: 2021/3/20
 * @author: cakin
 */
public class ArrBinaryTreeDemo {

    /**
     * 功能描述：顺序存储二叉树测试
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/3/20
     */
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7};
        // 创建一个顺序存储二叉树
        ArrBinaryTree arrBinaryTree = new ArrBinaryTree(arr);
        arrBinaryTree.preOrder(); // 1,2,4,5,3,6,7
    }

}

//编写一个ArrayBinaryTree, 实现顺序存储二叉树遍历

/**
 * @className: ArrBinaryTree
 * @description: 顺序存储二叉树数据结构，并实现前序遍历
 * @date: 2021/3/20
 * @author: cakin
 */
class ArrBinaryTree {
    // 存储顺序存储二叉树数据结点的数组
    private int[] arr;

    public ArrBinaryTree(int[] arr) {
        this.arr = arr;
    }

    // 重载preOrder
    public void preOrder() {
        this.preOrder(0);
    }

    /**
     * 功能描述：顺序存储二叉树的前序遍历
     *
     * @param index 数组的下标
     */
    public void preOrder(int index) {
        // 如果数组为空，或者 arr.length = 0，不能进行顺序存储二叉树的前序遍历
        if (arr == null || arr.length == 0) {
            System.out.println("数组为空，不能进行顺序存储二叉树的前序遍历");
        }
        // 输出当前这个元素
        System.out.println(arr[index]);
        // 向左递归遍历
        if ((index * 2 + 1) < arr.length) {
            preOrder(2 * index + 1);
        }
        // 右递归遍历
        if ((index * 2 + 2) < arr.length) {
            preOrder(2 * index + 2);
        }
    }
}
