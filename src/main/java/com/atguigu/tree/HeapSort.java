package com.atguigu.tree;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * @className: HeapSort
 * @description: 堆排序
 * @date: 2021/3/20
 * @author: cakin
 */
public class HeapSort {
    /**
     * 功能描述：堆排序测试
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/3/20
     */
    public static void main(String[] args) {
        // 将数组进行升序排序
/*        int arr1[] = {4, 6, 8, 7, 12, 45, 5, 9};
        heapSort(arr1);
        System.out.println("排序后：" + Arrays.toString(arr1));*/
        // 8000000个随机数，测试性能
        int[] arr = new int[8000000];
        for (int i = 0; i < 8000000; i++) {
            arr[i] = (int) (Math.random() * 8000000); // 生成一个[0, 8000000) 数
        }
        System.out.println("排序前");
        Date data1 = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date1Str = simpleDateFormat.format(data1);
        System.out.println("排序前的时间是=" + date1Str);
        heapSort(arr);
        Date data2 = new Date();
        String date2Str = simpleDateFormat.format(data2);
        System.out.println("排序后的时间是=" + date2Str);
    }

    /**
     * 功能描述：堆排序算法
     *
     * @param arr 待排序数组
     * @author cakin
     * @date 2021/3/20
     */
    public static void heapSort(int arr[]) {
        int temp;
        System.out.println("堆排序");

        // 将无序序列构建成一个堆，根据升序降序需求选择大顶堆或小顶堆
        for (int i = arr.length / 2 - 1; i >= 0; i--) {
            adjustHeap(arr, i, arr.length);
        }
        //System.out.println("调整后：" + Arrays.toString(arr));

		/*
		 * 将堆顶元素与末尾元素交换，将最大元素"沉"到数组末端;
		 * 重新调整结构，使其满足堆定义，然后继续交换堆顶元素与当前末尾元素，反复执行调整+交换步骤，直到整个序列有序。
		 */
        for (int j = arr.length - 1; j > 0; j--) {
            //交换
            temp = arr[j];
            arr[j] = arr[0];
            arr[0] = temp;
            adjustHeap(arr, 0, j);
            //System.out.println("调整后：" + Arrays.toString(arr));
        }
    }

    /**
     * 功能描述： 将以 i 对应的非叶子结点的树调整成大顶堆
     * 举例：
     * {4, 6, 8, 5, 9} => i = 1 => adjustHeap =>
     * {4, 9, 8, 5, 6} => i = 0 => adjustHeap
     * {9, 6, 8, 5, 4}
     *
     * @param arr    待调整的数组
     * @param i      表示非叶子结点在数组中索引
     * @param lenght 表示对多少个元素继续调整， length 是在逐渐的减少
     */
    public static void adjustHeap(int arr[], int i, int lenght) {
        int temp = arr[i]; // 先取出当前元素的值，保存在临时变量
        // 开始调整
        // k = i * 2 + 1 k 是 i结点的左子结点
        for (int k = i * 2 + 1; k < lenght; k = k * 2 + 1) {
            if (k + 1 < lenght && arr[k] < arr[k + 1]) { // 说明左子结点的值小于右子结点的值
                k++; // k 指向右子结点
            }
            if (arr[k] > temp) { // 如果子结点大于父结点
                arr[i] = arr[k]; // 把较大的值赋给当前结点
                i = k; // i 指向 k,继续循环比较
            } else {
                break;
            }
        }
        // 当for 循环结束后，我们已经将以 i 为父结点的树的最大值，放在了 最顶(局部)
        arr[i] = temp; // 将temp值放到调整后的位置
    }
}
