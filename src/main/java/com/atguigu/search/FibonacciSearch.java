package com.atguigu.search;

import java.util.Arrays;

/**
 * @className: FibonacciSearch
 * @description: 斐波那契(黄金分割法)查找
 * @date: 2021/3/14
 * @author: cakin
 */
public class FibonacciSearch {
    // 斐波那契数组长度
    public static int maxSize = 20;

    /**
     * 功能描述：斐波那契(黄金分割法)查找
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/3/14
     */
    public static void main(String[] args) {
        int[] arr = {1, 8, 10, 89, 1000, 1234};
        System.out.println("index=" + fibSearch(arr, 89));
    }

    /**
     * 功能描述：产生一个斐波那契数列
     *
     * @return int[] 斐波那契数列  {1, 1, 2, 3, 5, 8, 13, 21, 34, 55 }
     * @author cakin
     * @date 2021/3/14
     */
    public static int[] fib() {
        int[] f = new int[maxSize];
        f[0] = 1;
        f[1] = 1;
        for (int i = 2; i < maxSize; i++) {
            f[i] = f[i - 1] + f[i - 2];
        }
        return f;
    }

    /**
     * 功能描述：斐波那契查找算法——使用非递归的方式
     *
     * @param a   待查找数组
     * @param key 需要查找的关键字
     * @return int 找到：对应的下标 没找到：-1
     */
    public static int fibSearch(int[] a, int key) {
        int low = 0;
        int high = a.length - 1;
        int k = 0; // 表示斐波那契分割数值的下标
        int mid; // 存放mid值
        int f[] = fib(); // 获取到斐波那契数列
        // 获取到斐波那契分割数值的下标
        while (high > f[k] - 1) {
            k++;
        }
        // 因为 f[k] 值 可能大于 a 的 长度，因此我们需要使用Arrays类，构造一个新的数组 temp[]，不足的部分会使用0填充
        int[] temp = Arrays.copyOf(a, f[k]);
        // 实际上需求使用a数组最后的数填充 temp
        // 例如：temp = {1,8, 10, 89, 1000, 1234, 0, 0}  => {1,8, 10, 89, 1000, 1234, 1234, 1234,}
        for (int i = high + 1; i < temp.length; i++) {
            temp[i] = a[high];
        }

        // 使用while来循环处理，找到的数 key
        while (low <= high) { // 只要这个条件满足，就可以找
            mid = low + f[k - 1] - 1;
            if (key < temp[mid]) { // 向 mid 左边找
                high = mid - 1;
                /**
                 *   k-- 说明
                 *   1 全部元素 = 前面的元素 + 后边元素
                 *   2 f[k] = f[k-1] + f[k-2]
                 *   3 因为前面有 f[k-1]个元素,所以可以继续拆分 f[k-1] = f[k-2] + f[k-3]
                 *   4 本轮查询是在 f[k]-1 个数中进行，下次查询是在f[k-1]-1 个数中进行，所以是 k-1
                 */
                k--;
            } else if (key > temp[mid]) { // 向 mid 右边找
                low = mid + 1;
                /**
                 *    k -= 2 说明
                 *    1. 全部元素 = 前面的元素 + 后边元素
                 *    2. f[k] = f[k-1] + f[k-2]
                 *    3. 因为后面有f[k-2] 所以可以继续拆分 f[k-2] = f[k-3] + f[k-4]
                 *    4 轮查询是在 f[k]-1 个数中进行，下次查询是在f[k-2]-1 个数中进行，所以是 k-2
                 */
                k -= 2;
            } else { // 找到
                // 需要确定，返回的是哪个下标，这里返回较小者
                if (mid <= high) {
                    return mid;
                } else {
                    return high;
                }
            }
        }
        // 没找到
        return -1;
    }
}
