package com.atguigu.search;

/**
 * @className: SeqSearch
 * @description: 线性查找
 * @date: 2021/3/13
 * @author: cakin
 */
public class SeqSearch {
    /**
     * 功能描述：线性查找测试
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/3/13
     */
    public static void main(String[] args) {
        // 这里的数据是没有顺序的
        int arr[] = {1, 9, 11, -1, 34, 89};
        int index = seqSearch(arr, 11);
        if (index == -1) {
            System.out.println("没有找到到");
        } else {
            System.out.println("找到，下标为=" + index);
        }
    }

    /**
     * 功能描述：线性查找
     *
     * @param arr   待查找数组
     * @param value 待查找值
     * @author cakin
     * @date 2021/3/13
     * @description: 找到一个满足条件的值，就返回
     */
    public static int seqSearch(int[] arr, int value) {
        // 线性查找是逐一比对，发现有相同值，就返回下标
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                return i;
            }
        }
        return -1;
    }
}
