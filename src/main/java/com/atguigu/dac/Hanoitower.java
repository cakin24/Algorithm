package com.atguigu.dac;

/**
 * @className: Hanoitower
 * @description: 汉诺塔问题
 * @date: 2021/3/29
 * @author: cakin
 */
public class Hanoitower {
    /**
     * 功能描述：汉诺塔问题测试
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/3/29
     */
    public static void main(String[] args) {
        hanoiTower(5, 'A', 'B', 'C');
    }

    /**
     * 功能描述：使用分治算法求解汉诺塔问题：将 num 个盘，从 a 移到 c ，借助 b.假定从上到下盘的编号依次是1,2,3,4...
     *
     * @param num 盘子的个数
     * @param a   第1个盘子
     * @param b   第2个盘子
     * @param c   第3个盘子
     * @author cakin
     * @date 2021/3/29
     */
    public static void hanoiTower(int num, char a, char b, char c) {
        // 如果只有一个盘
        if (num == 1) {
            System.out.println("1 from " + a + " to " + c);
        } else { // 如果有 num >= 2 个盘子，总是可以看做是两个盘 1-最下边的1个盘 2-上面的的 num-1 个盘
            // 1 先把 上面的的 num-1 个盘从 a 移到到 b， 移动过程会使用到 c
            hanoiTower(num - 1, a, c, b);
            // 2 把最下边的盘 从 a 移到 c
            System.out.println(num + " from " + a + " to " + c);
            // 3 把 b 塔的所有盘 从 b 移到 c , 移动过程使用到 a
            hanoiTower(num - 1, b, a, c);
        }
    }
}
