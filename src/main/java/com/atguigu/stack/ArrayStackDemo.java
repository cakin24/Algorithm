package com.atguigu.stack;

import java.util.Scanner;

/**
 * @className: ArrayStackDemo
 * @description: 栈演示
 * @date: 2021/3/5
 * @author: cakin
 */
public class ArrayStackDemo {

    /**
     * 功能描述：栈测试
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/3/5
     */
    public static void main(String[] args) {

        // 创建一个ArrayStack对象，用来表示栈
        ArrayStack stack = new ArrayStack(4);
        String key;
        // 控制是否退出菜单
        boolean loop = true;
        Scanner scanner = new Scanner(System.in);
        while (loop) {
            System.out.println("show: 显示栈");
            System.out.println("exit: 退出程序");
            System.out.println("push: 添加数据到栈(入栈)");
            System.out.println("pop: 从栈取出数据(出栈)");
            System.out.println("请输入你的选择");
            key = scanner.next();
            switch (key) {
                case "show":
                    stack.list();
                    break;
                case "push":
                    System.out.println("请输入一个数");
                    int value = scanner.nextInt();
                    stack.push(value);
                    break;
                case "pop":
                    try {
                        int res = stack.pop();
                        System.out.printf("出栈的数据是 %d\n", res);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "exit":
                    scanner.close();
                    loop = false;
                    break;
                default:
                    break;
            }
        }
        System.out.println("程序退出");
    }
}

/**
 * @className: ArrayStack
 * @description: 栈的定义
 * @date: 2021/3/5
 * @author: cakin
 */
class ArrayStack {
    private int maxSize; // 栈的大小
    private int[] stack; // 数组，数组模拟栈，数据就放在该数组
    private int top = -1; // top表示栈顶，初始化为-1

    // 构造器
    public ArrayStack(int maxSize) {
        this.maxSize = maxSize;
        stack = new int[this.maxSize];
    }

    /**
     * 功能描述：栈满
     *
     * @author cakin
     * @date 2021/3/5
     */
    public boolean isFull() {
        return top == maxSize - 1;
    }

    /**
     * 功能描述：栈空
     *
     * @author cakin
     * @date 2021/3/5
     */
    public boolean isEmpty() {
        return top == -1;
    }

    /**
     * 功能描述：入栈
     *
     * @param value 入栈的值
     * @author cakin
     * @date 2021/3/5
     */
    public void push(int value) {
        // 判断栈是否满
        if (isFull()) {
            System.out.println("栈满");
            return;
        }
        top++;
        stack[top] = value;
    }

    /**
     * 功能描述：出栈, 将栈顶的数据返回
     *
     * @return int 栈顶的数据
     * @author cakin
     * @date 2021/3/5
     */
    public int pop() {
        // 判断栈是否空
        if (isEmpty()) {
            // 抛出异常
            throw new RuntimeException("栈空，没有数据~");
        }
        int value = stack[top];
        top--;
        return value;
    }

    /**
     * 功能描述：显示栈的数据， 遍历时，需要从栈顶开始显示数据
     *
     * @author cakin
     * @date 2021/3/5
     */
    public void list() {
        if (isEmpty()) {
            System.out.println("栈空，没有数据~~");
            return;
        }
        // 从栈顶开始显示数据
        for (int i = top; i >= 0; i--) {
            System.out.printf("stack[%d]=%d\n", i, stack[i]);
        }
    }
}
