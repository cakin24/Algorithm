package com.atguigu.stack;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @className: PolandNotation
 * @description: 逆波兰表达式求值
 * @date: 2021/3/6
 * @author: cakin
 */
public class PolandNotation {

    /**
     * 功能描述：逆波兰表达式求值测试
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/3/6
     * @description: 算法
     * 一 完成将一个中缀表达式转成后缀表达式的功能
     * 1 1+((2+3)×4)-5 => 转成  1 2 3 + 4 × + 5 –
     * 2 因为直接对 str 进行操作，不方便，因此 先将  "1+((2+3)×4)-5" 中缀的表达式转化为对应的List
     * "1+((2+3)×4)-5" => ArrayList [1,+,(,(,2,+,3,),*,4,),-,5]
     * 3 将得到的中缀表达式对应的List 转化为 后缀表达式对应的List
     * ArrayList [1,+,(,(,2,+,3,),*,4,),-,5]   => ArrayList [1,2,3,+,4,*,+,5,–]
     * 二 实现后缀表达式的计算
     */
    public static void main(String[] args) {
        // 字符串形式的中缀表达式
        String expression = "1+((2+3)*4)-5"; // 字符串形式的中缀表达式，最后结果是16
        // List形式的中缀表达式
        List<String> infixExpressionList = toInfixExpressionList(expression);
        System.out.println("中缀表达式对应的List=" + infixExpressionList); // ArrayList [1,+,(,(,2,+,3,),*,4,),-,5]
        // List形式的后缀表达式
        List<String> suffixExpreesionList = parseSuffixExpreesionList(infixExpressionList);
        System.out.println("后缀表达式对应的List" + suffixExpreesionList); // ArrayList [1,2,3,+,4,*,+,5,–]
        // 最终结算结果
        System.out.printf("expression=%d", calculate(suffixExpreesionList)); // ?
    }

    /**
     * 功能描述：将中缀表达式对应的List 转化为 后缀表达式对应的List，
     * ArrayList [1,+,(,(,2,+,3,),*,4,),-,5]  =》 ArrayList [1,2,3,+,4,*,+,5,–]，
     *
     * @param ls 中缀表达式对应的List
     * @return List<String> 后缀表达式对应的List
     * @author cakin
     * @date 2021/3/6
     * @description: 算法
     * 1 初始化两个栈：运算符栈s1和储存中间结果的栈s2。
     * 2 从左至右扫描中缀表达式。
     * 3 遇到操作数时，将其压s2；
     * 4 遇到运算符时，比较其与s1栈顶运算符的优先级：
     * 4.1 如果s1为空，或栈顶运算符为左括号“(”，则直接将此运算符入栈。
     * 4.2 否则，若优先级比栈顶运算符的高，也将运算符压入s1。
     * 4.3 否则，将s1栈顶的运算符弹出并压入到s2中，再次转到4.1与s1中新的栈顶运算符相比较。
     * 5 遇到括号时，分下面两种情况进行处理。
     * 5.1 如果是左括号“(”，则直接压入s1。
     * 5.2 如果是右括号“)”，则依次弹出s1栈顶的运算符，并压入s2，直到遇到左括号为止，此时将这一对括号丢弃。
     * 6 重复步骤2至5，直到表达式的最右边。
     * 7 将s1中剩余的运算符依次弹出并压入s2。
     * 8 依次弹出s2中的元素并输出，结果的逆序即为中缀表达式对应的后缀表达式。
     */
    public static List<String> parseSuffixExpreesionList(List<String> ls) {
        // 定义符号栈
        Stack<String> s1 = new Stack<>();
        // 因为s2 这个栈，在整个转换过程中，没有pop操作，而且后面我们还需要逆序输出，比较麻烦，这里就不用栈，直接使用 List<String> s2
        //Stack<String> s2 = new Stack<String>(); // 储存中间结果的栈s2
        List<String> s2 = new ArrayList<>(); // 储存中间结果的s2
        // 遍历ls
        for (String item : ls) {
            // 如果是一个数，加入s2
            if (item.matches("\\d+")) {
                s2.add(item);
            } else if (item.equals("(")) {
                s1.push(item);
            } else if (item.equals(")")) {
                // 如果是右括号“)”，则依次弹出s1栈顶的运算符，并压入s2，直到遇到左括号为止，此时将这一对括号丢弃
                while (!s1.peek().equals("(")) {
                    s2.add(s1.pop());
                }
                s1.pop(); // 将 ( 弹出 s1栈， 消除小括号
            } else {
                // 当item的优先级小于等于s1栈顶运算符, 将s1栈顶的运算符弹出并加入到s2中，再次转到(4.1)与s1中新的栈顶运算符相比较
                while (s1.size() != 0 && Operation.getValue(s1.peek()) >= Operation.getValue(item)) {
                    s2.add(s1.pop());
                }
                // 还需要将item压入栈
                s1.push(item);
            }
        }
        // 将s1中剩余的运算符依次弹出并加入s2
        while (s1.size() != 0) {
            s2.add(s1.pop());
        }
        //注意因为是存放到List, 因此按顺序输出就是对应的后缀表达式对应的List
        return s2;
    }

    /**
     * 功能描述：将字符串形式的中缀表达式转化为List形式的中缀表达式
     *
     * @param s 字符串形式的中缀表达式
     * @return List<String> List形式的中缀表达式
     * @author cakin
     * @date 2021/3/6
     */
    public static List<String> toInfixExpressionList(String s) {
        // 定义一个List,存放中缀表达式对应的内容
        List<String> ls = new ArrayList<>();
        // 一个指针，用于遍历中缀表达式字符串
        int i = 0;
        String str; // 对多位数的拼接
        char c; // 每遍历到一个字符，就放入到c
        do {
            // 如果c是一个非数字，需要加入到ls
            if ((c = s.charAt(i)) < 48 || (c = s.charAt(i)) > 57) {
                ls.add("" + c);
                i++; // i需要后移
            } else { // 如果是一个数，需要考虑多位数
                str = ""; // 先将str 置成""
                // '0'对应[48]，'9'对应[57]
                while (i < s.length() && (c = s.charAt(i)) >= 48 && (c = s.charAt(i)) <= 57) {
                    str += c; // 拼接多位数
                    i++;
                }
                ls.add(str);
            }
        } while (i < s.length());
        return ls;
    }

    /**
     * 功能描述：将一个逆波兰表达式， 依次将数据和运算符放入到 ArrayList中
     *
     * @param suffixExpression 字符串形式的逆波兰表达式
     * @return List<String> List形式的波兰表达式
     * @author cakin
     * @date 2021/3/6
     * @description:
     */
    public static List<String> getListString(String suffixExpression) {
        // 将 suffixExpression 分割
        String[] split = suffixExpression.split(" ");
        List<String> list = new ArrayList<>();
        for (String ele : split) {
            list.add(ele);
        }
        return list;
    }


	/*
	 * 1)从左至右扫描，将3和4压入堆栈；
		2)遇到+运算符，因此弹出4和3（4为栈顶元素，3为次顶元素），计算出3+4的值，得7，再将7入栈；
		3)将5入栈；
		4)接下来是×运算符，因此弹出5和7，计算出7×5=35，将35入栈；
		5)将6入栈；
		6)最后是-运算符，计算出35-6的值，即29，由此得出最终结果
	 */

    /**
     * 功能描述：逆波兰表达式的运算
     *
     * @param ls 字符串形式的逆波兰表达式
     * @return int 计算结果
     * @author cakin
     * @date 2021/3/6
     * @description: 算法
     * 1)从左至右扫描，将3和4压入堆栈。
     * 2)遇到+运算符，因此弹出4和3（4为栈顶元素，3为次顶元素），计算出3+4的值，得7，再将7入栈。
     * 3)将5入栈。
     * 4)接下来是×运算符，因此弹出5和7，计算出7×5=35，将35入栈。
     * 5)将6入栈。
     * 6)最后是-运算符，计算出35-6的值，即29，由此得出最终结果。
     */
    public static int calculate(List<String> ls) {
        // 创建给栈, 只需要一个栈即可
        Stack<String> stack = new Stack<>();
        // 遍历 ls
        for (String item : ls) {
            // 这里使用正则表达式来取出数
            if (item.matches("\\d+")) { // 匹配的是多位数
                // 入栈
                stack.push(item);
            } else {
                // pop出两个数，并运算， 再入栈
                int num2 = Integer.parseInt(stack.pop());
                int num1 = Integer.parseInt(stack.pop());
                int res;
                if (item.equals("+")) {
                    res = num1 + num2;
                } else if (item.equals("-")) {
                    res = num1 - num2;
                } else if (item.equals("*")) {
                    res = num1 * num2;
                } else if (item.equals("/")) {
                    res = num1 / num2;
                } else {
                    throw new RuntimeException("运算符有误");
                }
                // 把res 入栈
                stack.push("" + res);
            }

        }
        // 最后留在stack中的数据是运算结果
        return Integer.parseInt(stack.pop());
    }
}

/**
 * @className: Operation
 * @description: 运算符对应的优先级
 * @date: 2021/3/6
 * @author: cakin
 */
class Operation {
    private static int ADD = 1;
    private static int SUB = 1;
    private static int MUL = 2;
    private static int DIV = 2;

    /**
     * 功能描述：获取对应运算符的优先级
     *
     * @param operation 运算符
     * @return int 优先级
     * @author cakin
     * @date 2021/3/6
     */
    public static int getValue(String operation) {
        int result = 0;
        switch (operation) {
            case "+":
                result = ADD;
                break;
            case "-":
                result = SUB;
                break;
            case "*":
                result = MUL;
                break;
            case "/":
                result = DIV;
                break;
            default:
                System.out.println("不存在该运算符" + operation);
                break;
        }
        return result;
    }
}
