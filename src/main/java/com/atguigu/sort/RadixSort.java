package com.atguigu.sort;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * @className: RadixSort
 * @description: 基数排序
 * @date: 2021/3/13
 * @author: cakin
 */
public class RadixSort {
    /**
     * 功能描述：基数排序测试
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/3/13
     */
    public static void main(String[] args) {
        // 待排序的数
    	// int arr[] = {53, 3, 542, 748, 14, 214};

        // 说明：如果是80000000个数进行排序，耗费空间3.3G，计算过程：80000000 * 11 * 4 / 1024 / 1024 / 1024 = 3.3G，会出现内存溢出
		// 8000000个数排序，只要1秒，典型的空间换时间
		int[] arr = new int[8000000];
		for (int i = 0; i < 8000000; i++) {
			arr[i] = (int) (Math.random() * 8000000); // 生成一个[0, 8000000) 数
		}
        // System.out.println("排序前");
        Date data1 = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date1Str = simpleDateFormat.format(data1);
        System.out.println("排序前的时间是=" + date1Str);
        radixSort(arr);
        Date data2 = new Date();
        String date2Str = simpleDateFormat.format(data2);
        System.out.println("排序后的时间是=" + date2Str);
        // System.out.println("基数排序后 " + Arrays.toString(arr));
    }

    //基数排序方法
    public static void radixSort(int[] arr) {
        // 1 得到数组中最大的数的位数
        int max = arr[0]; // 假设第一数就是最大数
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        // 得到最大数是几位数
        int maxLength = (max + "").length();

        // 定义一个二维数组，表示10个桶, 每个桶就是一个一维数组
        // 1 二维数组包含10个一维数组
        // 2 为了防止在放入数的时候，数据溢出，则每个一维数组(桶)，大小定为arr.length
        // 3 基数排序是使用空间换时间的经典算法
        int[][] bucket = new int[10][arr.length];

        // 为了记录每个桶中实际存放了多少个数据,我们定义一个一维数组来记录各个桶的每次放入的数据个数
        // 比如：bucketElementCounts[0] , 记录的就是  bucket[0] 桶的放入数据个数
        int[] bucketElementCounts = new int[10];

		// 针对每个元素的对应位进行排序处理， 第一次是个位，第二次是十位，第三次是百位..
        for (int i = 0, n = 1; i < maxLength; i++, n *= 10) {
            for (int j = 0; j < arr.length; j++) {
                // 取出每个元素的对应位的值
                int digitOfElement = arr[j] / n % 10;
                // 放入到对应的桶中
                bucket[digitOfElement][bucketElementCounts[digitOfElement]] = arr[j];
                bucketElementCounts[digitOfElement]++;
            }
            // 一维数组的下标依次取出数据，放入原来数组
            int index = 0;
            // 遍历每一桶，并将桶中是数据，放入到原数组
            for (int k = 0; k < bucketElementCounts.length; k++) {
                // 如果桶中有数据，才放入到原数组
                if (bucketElementCounts[k] != 0) {
                    // 循环该桶即第k个桶(即第k个一维数组), 放入
                    for (int l = 0; l < bucketElementCounts[k]; l++) {
                        // 取出元素放入到arr
                        arr[index++] = bucket[k][l];
                    }
                }
                // 第i轮处理后，需要将每个 bucketElementCounts[k] 置0
                bucketElementCounts[k] = 0;
            }
            // System.out.println("第"+(i+1)+"轮，对个位的排序处理 arr =" + Arrays.toString(arr));
        }
    }
}
