package com.atguigu.sort;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @className: InsertSort
 * @description: 插入排序
 * @date: 2021/3/8
 * @author: cakin
 */
public class InsertSort {
    /**
     * 功能描述：插入排序测试
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/3/8
     */
    public static void main(String[] args) {
        // 待排序的数组
        // int[] arr = {101, 34, 119, 1, -1, 89};
        // 80000个的随机数
        int[] arr = new int[80000];
        for (int i = 0; i < 80000; i++) {
            arr[i] = (int) (Math.random() * 8000000); // 生成一个[0, 8000000) 数
        }
        // System.out.println("插入排序前");
        Date data1 = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date1Str = simpleDateFormat.format(data1);
        System.out.println("排序前的时间是=" + date1Str);
        // 插入排序算法
        insertSort(arr);
        Date data2 = new Date();
        String date2Str = simpleDateFormat.format(data2);
        System.out.println("排序前的时间是=" + date2Str);
        // System.out.println(Arrays.toString(arr));
    }

    /**
     * 功能描述：插入排序
     *
     * @param arr 待排序数组
     * @author cakin
     * @date 2021/3/8
     */
    public static void insertSort(int[] arr) {
        int insertVal;
        int insertIndex;
        for (int i = 1; i < arr.length; i++) {
            // 待插入的数
            insertVal = arr[i];
            insertIndex = i - 1; // 即arr[i]前面这个数的下标


            /**
             *  给insertVal 找到插入的位置
             *  1 insertIndex >= 0 保证在给insertVal 找插入位置，不越界
             *  2 insertVal < arr[insertIndex] 待插入的数小于当前比较的数，说明还没有找到插入位置
             *  3 将 arr[insertIndex] 后移
             */
            while (insertIndex >= 0 && insertVal < arr[insertIndex]) {
                arr[insertIndex + 1] = arr[insertIndex];
                insertIndex--;
            }
            // 当退出while循环时，说明插入的位置找到, 即 insertIndex + 1 这个位置
            // 只有 insertIndex 发生了变化，就说明挪动了位置，这个时候才去插入
            if (insertIndex + 1 != i) {
                arr[insertIndex + 1] = insertVal;
            }
            // System.out.println("第"+i+"轮插入");
            // System.out.println(Arrays.toString(arr));
        }
    }
}
