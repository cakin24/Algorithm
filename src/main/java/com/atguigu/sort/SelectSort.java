package com.atguigu.sort;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @className: SelectSort
 * @description: 选择排序
 * @date: 2021/3/8
 * @author: cakin
 */
public class SelectSort {
    /**
     * 功能描述：选择排序测试
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/3/8
     */
    public static void main(String[] args) {
    	// 待测试数字
        // int [] arr = {101, 34, 119, 1, -1, 90, 123};

        // 创建80000个的随机数
        int[] arr = new int[80000];
        for (int i = 0; i < 80000; i++) {
            arr[i] = (int) (Math.random() * 8000000); // 生成一个[0, 8000000) 数
        }

        // System.out.println("排序前");
        // System.out.println(Arrays.toString(arr));
        Date data1 = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date1Str = simpleDateFormat.format(data1);
        System.out.println("排序前的时间是=" + date1Str);
		// 选择排序
        selectSort(arr);
        Date data2 = new Date();
        String date2Str = simpleDateFormat.format(data2);
        System.out.println("排序后的时间是=" + date2Str);
        // System.out.println("排序后");
        // System.out.println(Arrays.toString(arr));
    }

	/**
	 * 功能描述：选择排序 选择排序时间复杂度是 O(n^2)
	 *
	 * @author cakin
	 * @date 2021/3/8
	 * @param arr 待排序数组
	 */
    public static void selectSort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            int minIndex = i;
            int min = arr[i];
            for (int j = i + 1; j < arr.length; j++) {
                if (min > arr[j]) {
                    min = arr[j]; // 保留最小值
                    minIndex = j; // 保留最小值索引
                }
            }
            // 将当前arr[i]值和最小值min进行交互
            if (minIndex != i) {
                arr[minIndex] = arr[i];
                arr[i] = min;
            }
            // System.out.println("第"+(i+1)+"轮后~~");
            // System.out.println(Arrays.toString(arr));// 1, 34, 119, 101
        }
    }
}
