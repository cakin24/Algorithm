package com.atguigu.sort;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * @className: QuickSort
 * @description: 快速排序
 * @date: 2021/3/12
 * @author: 贝医
 */
public class QuickSort {
    /**
     * 功能描述：快速排序测试
     *
     * @param args 命令行
     * @author 贝医
     * @date 2021/3/12
     */
    public static void main(String[] args) {
        // 待排序的数
        // int[] arr = {-9,78,0,23,-567,70, -1,900, 4561};
        // 测试快速排序的执行速度
        // 8000000个的随机数
        int[] arr = new int[8000000];
        for (int i = 0; i < 8000000; i++) {
            arr[i] = (int) (Math.random() * 8000000); // 生成一个[0, 8000000) 数
        }
        // System.out.println("排序前");
        Date data1 = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date1Str = simpleDateFormat.format(data1);
        System.out.println("排序前的时间是=" + date1Str);
        quickSort(arr, 0, arr.length - 1);
        Date data2 = new Date();
        String date2Str = simpleDateFormat.format(data2);
        System.out.println("排序后的时间是=" + date2Str);
        //System.out.println("arr=" + Arrays.toString(arr));
    }

    public static void quickSort(int[] arr, int left, int right) {
        int l = left; // 左下标  0
        int r = right; // 右下标 8
        // pivot 中轴值
        int pivot = arr[(left + right) / 2];
        int temp; //临时变量，作为交换时使用
        // while循环的目的是让比 pivot（-567） 值小放到左边，比pivot（-567） 值大放到右边
        while (l < r) {
            // 在 pivot 的左边一直找,找到大于等于 pivot 值,才退出
            while (arr[l] < pivot) {
                l += 1;
            }
            // 在pivot的右边一直找,找到小于等于 pivot 值,才退出
            while (arr[r] > pivot) {
                r -= 1;
            }
            // 如果l >= r说明pivot 的左右两的值，已经按照左边全部是小于等于pivot值，右边全部是大于等于pivot值
            if (l >= r) {
                break;
            }
            // 交换
            temp = arr[l];
            arr[l] = arr[r];
            arr[r] = temp;
            // 如果交换完后，发现这个arr[l] == pivot值 相等 r--， 前移
            if (arr[l] == pivot) {
                r -= 1;
            }
            // 如果交换完后，发现这个arr[r] == pivot值 相等 l++， 后移
            if (arr[r] == pivot) {
                l += 1;
            }
        }
        // 如果 l == r, 必须l++, r--, 否则为出现栈溢出
        if (l == r) {
            l += 1;
            r -= 1;
        }
        // 前面的一切都是为了确定中轴值得最终位置值，也就是得出 r 和 l
        // 向左递归
        if (left < r) {
            quickSort(arr, left, r);
        }
        // 向右递归
        if (right > l) {
            quickSort(arr, l, right);
        }
    }
}
