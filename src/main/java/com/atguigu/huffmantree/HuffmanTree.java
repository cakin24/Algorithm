package com.atguigu.huffmantree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @className: HuffmanTree
 * @description: 赫夫曼树创建
 * @date: 2021/3/21
 * @author: cakin
 */
public class HuffmanTree {
    /**
     * 功能描述：赫夫曼树创建测试
     *
     * @param args 命令行
     * @author cakin
     * @date 2021/3/21
     */
    public static void main(String[] args) {
        int arr[] = {13, 7, 8, 3, 29, 6, 1};
        Node root = createHuffmanTree(arr);
        // 测试
        preOrder(root);
    }

    /**
     * 功能描述：前序遍历的方法
     *
     * @param root 根节点
     * @author 贝医
     * @date 2021/3/21
     */
    public static void preOrder(Node root) {
        if (root != null) {
            root.preOrder();
        } else {
            System.out.println("是空树，不能遍历。");
        }
    }

    /**
     * 功能描述：创建赫夫曼树
     *
     * @param arr 需要创建成哈夫曼树的数组
     * @return 创建好后的赫夫曼树的root结点
     */
    public static Node createHuffmanTree(int[] arr) {
        /**
         * 1 遍历 arr 数组
         * 2 将 arr 的每个元素构成成一个 Node
         * 3 将Node 放入到 ArrayList 中
         */
        List<Node> nodes = new ArrayList<>();
        for (int value : arr) {
            nodes.add(new Node(value));
        }

        // 创建创建好后的赫夫曼树
        while (nodes.size() > 1) {
            // 从小到大排序
            Collections.sort(nodes);
            // 排序后的节点
            System.out.println("nodes =" + nodes);

            // 取出根节点权值最小的两颗二叉树
            // 取出权值最小的结点
            Node leftNode = nodes.get(0);
            // 取出权值第二小的结点
            Node rightNode = nodes.get(1);
            // 构建一颗新的二叉树
            Node parent = new Node(leftNode.value + rightNode.value);
            parent.left = leftNode;
            parent.right = rightNode;
            // 从ArrayList删除处理过的二叉树
            nodes.remove(leftNode);
            nodes.remove(rightNode);
            // 将parent加入到nodes
            nodes.add(parent);
        }
        // 返回哈夫曼树的root结点
        return nodes.get(0);
    }
}

/**
 * @className: HuffmanTree
 * @description: 结点类：为了排序，实现 Comparable 接口
 * @date: 2021/3/21
 * @author: cakin
 */
class Node implements Comparable<Node> {
    int value; // 结点权值
    Node left; // 指向左子结点
    Node right; // 指向右子结点

    /**
     * 功能描述：前序遍历
     *
     * @author cakin
     * @date 2021/3/21
     */
    public void preOrder() {
        System.out.println(this);
        if (this.left != null) {
            this.left.preOrder();
        }
        if (this.right != null) {
            this.right.preOrder();
        }
    }

    public Node(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Node [value=" + value + "]";
    }

    /**
     * 功能描述：实现节点从小到大排序
     *
     * @author 贝医
     * @date 2021/3/21
     * @param o 节点
     */
    @Override
    public int compareTo(Node o) {
        // 表示从小到大排序
        return this.value - o.value;
    }
}
